import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day21Test {
    private static final List<String> exampleInput = Arrays.asList("""
            mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
            trh fvjkl sbzzf mxmxvkd (contains dairy)
            sqjhc fvjkl (contains soy)
            sqjhc mxmxvkd sbzzf (contains fish)""".stripIndent().split("\n"));

    @Test
    void exampleOne() {
        Day21.FoodList foodList = new Day21.FoodList(exampleInput);
        assertEquals(Map.of(
                "dairy", Set.of("mxmxvkd"),
                "fish", Set.of("sqjhc", "mxmxvkd"),
                "soy", Set.of("sqjhc", "fvjkl")
                ),
                foodList.allergicCandidates
        );
        Set<String> nonAllergicIngredients = foodList.findNonAllergicIngredients();
        assertEquals(
                Set.of("kfcds", "nhms", "sbzzf", "trh"),
                nonAllergicIngredients
        );

        assertEquals(
                5,
                nonAllergicIngredients.stream()
                        .mapToLong(foodList::countFoods)
                        .sum()
        );
    }

    @Test
    void solutionOne() {
        Day21.FoodList foodList = new Day21.FoodList(readStrings(this));
        assertEquals(
                1885,
                foodList.findNonAllergicIngredients().stream()
                        .mapToLong(foodList::countFoods)
                        .sum()
        );
    }

    @Test
    void exampleTwo() {
        Day21.FoodList foodList = new Day21.FoodList(exampleInput);
        List<String> allergicIngredients = foodList.findAllergicIngredients();
        assertEquals(List.of("mxmxvkd", "sqjhc", "fvjkl"), allergicIngredients);
    }

    @Test
    void solutionTwo() {
        Day21.FoodList foodList = new Day21.FoodList(readStrings(this));
        List<String> allergicIngredients = foodList.findAllergicIngredients();
        assertEquals("fllssz,kgbzf,zcdcdf,pzmg,kpsdtv,fvvrc,dqbjj,qpxhfp", String.join(",", allergicIngredients));
    }
}