package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.List;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day11Test {

    private final List<String> exampleInput = List.of(
            "L.LL.LL.LL",
            "LLLLLLL.LL",
            "L.L.L..L..",
            "LLLL.LL.LL",
            "L.LL.LL.LL",
            "L.LLLLL.LL",
            "..L.L.....",
            "LLLLLLLLLL",
            "L.LLLLLL.L",
            "L.LLLLL.LL"
    );

    @Test
    void exampleOneStep() {
        final Day11.Grid grid = Day11.Grid.parse(exampleInput);
        assertEquals(String.join("\n", exampleInput), grid.toString());

        grid.step();
        assertEquals("""
            #.##.##.##
            #######.##
            #.#.#..#..
            ####.##.##
            #.##.##.##
            #.#####.##
            ..#.#.....
            ##########
            #.######.#
            #.#####.##""".stripIndent(), grid.toString());

        grid.step();
        assertEquals("""
            #.LL.L#.##
            #LLLLLL.L#
            L.L.L..L..
            #LLL.LL.L#
            #.LL.LL.LL
            #.LLLL#.##
            ..L.L.....
            #LLLLLLLL#
            #.LLLLLL.L
            #.#LLLL.##""".stripIndent(), grid.toString());
    }

    @Test
    void exampleOne() {
        final Day11.Grid grid = Day11.getStabilizedGrid(Day11.Grid.parse(exampleInput));
        assertEquals(37, grid.countOccupiedSeats());
    }

    @Test
    void solutionOne() {
        final Day11.Grid grid = Day11.getStabilizedGrid(Day11.Grid.parse(readStrings(this)));
        assertEquals(2329, grid.countOccupiedSeats());
    }

    @Test
    void exampleTwo() {
        final Day11.Grid grid = Day11.getStabilizedGrid(
                Day11.Grid.parse(exampleInput)
                .setNeighborMode(Day11.NeighborMode.LINE_OF_SIGHT)
                .setAcceptableNeighbors(5)
        );
        assertEquals(26, grid.countOccupiedSeats());
    }

    @Test
    void solutionTwo() {
        final Day11.Grid grid = Day11.getStabilizedGrid(
                Day11.Grid.parse(readStrings(this))
                        .setNeighborMode(Day11.NeighborMode.LINE_OF_SIGHT)
                        .setAcceptableNeighbors(5)
        );
        assertEquals(2138, grid.countOccupiedSeats());
    }
}