package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.*;

class Day12Test {
    private static final List<String> exampleInput = Arrays.asList("""
        F10
        N3
        F7
        R90
        F11""".stripIndent().split("\n").clone());

    @Test
    void exampleOne() {
        final Day12.Ship ship = new Day12.Navigation(exampleInput).run();
        assertEquals(25, ship.position.getManhattanDistance());
    }

    @Test
    void solutionOne() {
        final Day12.Ship ship = new Day12.Navigation(readStrings(this)).run();
        assertEquals(796, ship.position.getManhattanDistance());
    }

    @Test
    void exampleTwo() {
        final Day12.Ship ship = new Day12.Navigation(exampleInput, true).run();
        assertEquals(286, ship.position.getManhattanDistance());
    }

    @Test
    void solutionTwo() {
        final Day12.Ship ship = new Day12.Navigation(readStrings(this), true).run();
        assertEquals(39446, ship.position.getManhattanDistance());
    }
}