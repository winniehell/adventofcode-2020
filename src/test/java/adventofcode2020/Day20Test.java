package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static adventofcode2020.Helper.readStrings;
import static adventofcode2020.Utils.getColumn;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day20Test {

    private static final List<String> exampleInput = Arrays.asList("""
            Tile 2311:
            ..##.#..#.
            ##..#.....
            #...##..#.
            ####.#...#
            ##.##.###.
            ##...#.###
            .#.#.#..##
            ..#....#..
            ###...#.#.
            ..###..###
                        
            Tile 1951:
            #.##...##.
            #.####...#
            .....#..##
            #...######
            .##.#....#
            .###.#####
            ###.##.##.
            .###....#.
            ..#.#..#.#
            #...##.#..
                        
            Tile 1171:
            ####...##.
            #..##.#..#
            ##.#..#.#.
            .###.####.
            ..###.####
            .##....##.
            .#...####.
            #.##.####.
            ####..#...
            .....##...
                        
            Tile 1427:
            ###.##.#..
            .#..#.##..
            .#.##.#..#
            #.#.#.##.#
            ....#...##
            ...##..##.
            ...#.#####
            .#.####.#.
            ..#..###.#
            ..##.#..#.
                        
            Tile 1489:
            ##.#.#....
            ..##...#..
            .##..##...
            ..#...#...
            #####...#.
            #..#.#.#.#
            ...#.#.#..
            ##.#...##.
            ..##.##.##
            ###.##.#..
                        
            Tile 2473:
            #....####.
            #..#.##...
            #.##..#...
            ######.#.#
            .#...#.#.#
            .#########
            .###.#..#.
            ########.#
            ##...##.#.
            ..###.#.#.
                        
            Tile 2971:
            ..#.#....#
            #...###...
            #.#.###...
            ##.##..#..
            .#####..##
            .#..####.#
            #..#.#..#.
            ..####.###
            ..#.#.###.
            ...#.#.#.#
                        
            Tile 2729:
            ...#.#.#.#
            ####.#....
            ..#.#.....
            ....#..#.#
            .##..##.#.
            .#.####...
            ####.#.#..
            ##.####...
            ##..#.##..
            #.##...##.
                        
            Tile 3079:
            #.#.#####.
            .#..######
            ..#.......
            ######....
            ####.#..#.
            .#...#.##.
            #.#####.##
            ..#.###...
            ..#.......
            ..#.###...""".stripIndent().split("\n"));

    @Test
    void exampleOne() {
        Day20.ImageTiles imageTiles = new Day20.ImageTiles(exampleInput);

        Day20.Tile tile1489 = imageTiles.tileMap.get(1489);
        assertEquals("""
                Tile 1489:
                ##.#.#....
                ..##...#..
                .##..##...
                ..#...#...
                #####...#.
                #..#.#.#.#
                ...#.#.#..
                ##.#...##.
                ..##.##.##
                ###.##.#..""".stripIndent(), tile1489.toString());

        assertEquals("#...##.#.#", getColumn(tile1489.grid, 0));
        assertEquals(".....#..#.", getColumn(tile1489.grid, 9));

        Day20.Tile tile1171 = imageTiles.tileMap.get(1171);
        assertEquals(Map.of(
                Day20.Side.TOP, "####...##.",
                Day20.Side.LEFT, "###....##.",
                Day20.Side.RIGHT, ".#..#.....",
                Day20.Side.BOTTOM, ".....##..."
        ), tile1171.edges);

        assertEquals(
                Set.of(2971, 1427, 1171),
                imageTiles.neighborMap.get(tile1489.id).stream()
                        .map(tile -> tile.id)
                        .collect(Collectors.toSet())
        );

        assertEquals(Set.of(1951, 1171, 2971, 3079), imageTiles.getCornerTiles());
    }

    @Test
    void solutionOne() {
        Day20.ImageTiles imageTiles = new Day20.ImageTiles(readStrings(this));
        imageTiles.arrange();
        Set<Integer> cornerTiles = imageTiles.getCornerTiles();
        assertEquals(Set.of(1511, 1723, 2221, 2287), cornerTiles);
        assertEquals(13224049461431L, cornerTiles
                .stream()
                .mapToLong(i -> i)
                .reduce(1, (a, b) -> a * b)
        );
    }

    @Test
    void rotateGrid() {
        List<String> grid = List.of(
                "abcdef",
                "123456",
                "uvwxyz"
        );
        assertEquals("""
                        u1a
                        v2b
                        w3c
                        x4d
                        y5e
                        z6f""".stripIndent(),
                String.join("\n", Utils.rotateGrid(grid))
        );
    }

    @Test
    void reverseList() {
        assertEquals(
                Arrays.asList("654321".split("")),
                Utils.reverseList(Arrays.asList("123456".split("")))
        );
    }

    @Test
    void exampleTwo() {
        Day20.ImageTiles imageTiles = new Day20.ImageTiles(exampleInput);
        Day20.Tile tile3079 = imageTiles.tileMap.get(3079);
        assertEquals("""
                        #..#####
                        .#......
                        #####...
                        ###.#..#
                        #...#.##
                        .#####.#
                        .#.###..
                        .#......""".stripIndent(),
                String.join("\n", tile3079.withoutEdges())
        );

        Day20.ArrangedTiles arrangedTiles = imageTiles.arrange();
        assertEquals("""
                        (0,0):1171 (1,0):2473 (2,0):3079
                        (0,1):1489 (1,1):1427 (2,1):2311
                        (0,2):2971 (1,2):2729 (2,2):1951""".stripIndent(),
                arrangedTiles.toString()
        );

        Day20.CombinedImage combinedImage = arrangedTiles.combine();
        combinedImage.rotate();
        combinedImage.flip();
        assertEquals("""
                        .#.#..#.##...#.##..#####
                        ###....#.#....#..#......
                        ##.##.###.#.#..######...
                        ###.#####...#.#####.#..#
                        ##.#....#.##.####...#.##
                        ...########.#....#####.#
                        ....#..#...##..#.#.###..
                        .####...#..#.....#......
                        #..#.##..#..###.#.##....
                        #.####..#.####.#.#.###..
                        ###.#.#...#.######.#..##
                        #.####....##..########.#
                        ##..##.#...#...#.#.#.#..
                        ...#..#..#.#.##..###.###
                        .#.#....#.##.#...###.##.
                        ###.#...#..#.##.######..
                        .#.#.###.##.##.#..#.##..
                        .####.###.#...###.#..#.#
                        ..#.#..#..#.#.#.####.###
                        #..####...#.#.#.###.###.
                        #####..#####...###....##
                        #.##..#..#...#..####...#
                        .#.###..##..##..####.##.
                        ...###...##...#...#..###""".stripIndent(),
                String.join("\n", combinedImage.grid)
        );

        assertEquals(273, combinedImage.calculateWaterRoughness());
    }

    @Test
    void solutionTwo() {
        Day20.ImageTiles imageTiles = new Day20.ImageTiles(readStrings(this));
        Day20.ArrangedTiles arrangedTiles = imageTiles.arrange();
        Day20.CombinedImage combinedImage = arrangedTiles.combine();
        assertEquals(2231, combinedImage.calculateWaterRoughness());
    }
}