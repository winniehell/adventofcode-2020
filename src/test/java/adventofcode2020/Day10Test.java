package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;

import static adventofcode2020.Helper.readIntegers;
import static org.junit.jupiter.api.Assertions.*;

class Day10Test {

    private final List<Integer> exampleInputSmall = List.of(
            16,
            10,
            15,
            5,
            1,
            11,
            7,
            19,
            6,
            12,
            4
    );

    private final List<Integer> exampleInputLarger = List.of(
            28,
            33,
            18,
            42,
            31,
            14,
            46,
            20,
            48,
            47,
            24,
            23,
            49,
            45,
            19,
            38,
            39,
            11,
            1,
            32,
            25,
            35,
            8,
            17,
            7,
            9,
            4,
            2,
            34,
            10,
            3
    );

    @Test
    void exampleOneSmall() {
        final SortedMap<Integer, Integer> differenceMap = new Day10(exampleInputSmall).getJoltageDifferenceMap();
        assertEquals(Map.of(1, 7, 3, 5), differenceMap);
    }

    @Test
    void exampleOneLarger() {
        final SortedMap<Integer, Integer> differenceMap = new Day10(exampleInputLarger).getJoltageDifferenceMap();
        assertEquals(Map.of(1, 22, 3, 10), differenceMap);
    }

    @Test
    void solutionOne() {
        final SortedMap<Integer, Integer> differenceMap = new Day10(readIntegers(this)).getJoltageDifferenceMap();
        assertEquals(2432, differenceMap.get(1) * differenceMap.get(3));
    }

    @Test
    void exampleTwoSmall() {
        final long numArrangements = new Day10(exampleInputSmall).countPossibleArrangements();
        assertEquals(8, numArrangements);
    }

    @Test
    void exampleTwoLarger() {
        final long numArrangements = new Day10(exampleInputLarger).countPossibleArrangements();
        assertEquals(19208, numArrangements);
    }

    @Test
    void solutionTwo() {
        final long numArrangements = new Day10(readIntegers(this)).countPossibleArrangements();
        assertEquals(453551299002368L, numArrangements);
    }
}