package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.List;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day02Test {
    List<String> exampleInput = List.of(
            "1-3 a: abcde",
            "1-3 b: cdefg",
            "2-9 c: ccccccccc"
    );

    @Test
    public void example1() {
        assertEquals(2, Day02.checkCharacterCountList(exampleInput));
    }

    @Test
    public void solution1() {
        final List<String> input = readStrings(this);
        assertEquals(439, Day02.checkCharacterCountList(input));
    }

    @Test
    public void example2(){
        assertEquals(1, Day02.checkCharacterPositionList(exampleInput));
    }

    @Test
    public void solution2() {
        final List<String> input = readStrings(this);
        assertEquals(584, Day02.checkCharacterPositionList(input));
    }
}
