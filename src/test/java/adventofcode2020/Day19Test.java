package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day19Test {

    private static final List<String> smallInput = Arrays.asList("""
            0: 1 2
            1: "a"
            2: 1 3 | 3 1
            3: "b"
                        
            aab
            aba""".stripIndent().split("\n"));

    private static final List<String> bigInput = Arrays.asList("""
            0: 4 1 5
            1: 2 3 | 3 2
            2: 4 4 | 5 5
            3: 4 5 | 5 4
            4: "a"
            5: "b"
                        
            ababbb
            bababa
            abbbab
            aaabbb
            aaaabbb""".stripIndent().split("\n"));

    private static final List<String> exampleInputTwo = Arrays.asList("""
            42: 9 14 | 10 1
            9: 14 27 | 1 26
            10: 23 14 | 28 1
            1: "a"
            11: 42 31
            5: 1 14 | 15 1
            19: 14 1 | 14 14
            12: 24 14 | 19 1
            16: 15 1 | 14 14
            31: 14 17 | 1 13
            6: 14 14 | 1 14
            2: 1 24 | 14 4
            0: 8 11
            13: 14 3 | 1 12
            15: 1 | 14
            17: 14 2 | 1 7
            23: 25 1 | 22 14
            28: 16 1
            4: 1 1
            20: 14 14 | 1 15
            3: 5 14 | 16 1
            27: 1 6 | 14 18
            14: "b"
            21: 14 1 | 1 14
            25: 1 1 | 1 14
            22: 14 14
            8: 42
            26: 14 22 | 1 20
            18: 15 15
            7: 14 5 | 1 21
            24: 14 1
                        
            abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
            bbabbbbaabaabba
            babbbbaabbbbbabbbbbbaabaaabaaa
            aaabbbbbbaaaabaababaabababbabaaabbababababaaa
            bbbbbbbaaaabbbbaaabbabaaa
            bbbababbbbaaaaaaaabbababaaababaabab
            ababaaaaaabaaab
            ababaaaaabbbaba
            baabbaaaabbaaaababbaababb
            abbbbabbbbaaaababbbbbbaaaababb
            aaaaabbaabaaaaababaa
            aaaabbaaaabbaaa
            aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
            babaaabbbaaabaababbaabababaaab
            aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba""".stripIndent().split("\n"));

    @Test
    void exampleOne() {
        final Day19 small = new Day19(smallInput);
        assertEquals(small.messages.size(), small.getValidMessages().size());

        final Day19 big = new Day19(bigInput);

        assertEquals(List.of(0, 2), IntStream.range(0, big.messages.size())
                .filter(i -> new Day19.MessageInterpreter(big.rules).isValid(big.messages.get(i)))
                .boxed()
                .collect(Collectors.toList())
        );

        assertEquals(2, big.getValidMessages().size());
    }

    @Test
    void solutionOne() {
        assertEquals(126, new Day19(readStrings(this)).getValidMessages().size());
    }

    @Test
    void exampleTwoWithoutPatch() {
        final Day19 withoutPatch = new Day19(exampleInputTwo);
        assertEquals(
                List.of("bbabbbbaabaabba", "ababaaaaaabaaab", "ababaaaaabbbaba"),
                withoutPatch.getValidMessages()
        );
    }

    @Test
    void exampleTwo() {
        final Day19 withPatch = new Day19(exampleInputTwo);
        withPatch.rules.put(8, "42 | 42 8");
        withPatch.rules.put(11, "42 31 | 42 11 31");
        assertEquals("""
                        bbabbbbaabaabba
                        babbbbaabbbbbabbbbbbaabaaabaaa
                        aaabbbbbbaaaabaababaabababbabaaabbababababaaa
                        bbbbbbbaaaabbbbaaabbabaaa
                        bbbababbbbaaaaaaaabbababaaababaabab
                        ababaaaaaabaaab
                        ababaaaaabbbaba
                        baabbaaaabbaaaababbaababb
                        abbbbabbbbaaaababbbbbbaaaababb
                        aaaaabbaabaaaaababaa
                        aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
                        aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba""".stripIndent(),
                String.join("\n", withPatch.getValidMessages())
        );
    }

    @Test
    void solutionTwo() {
        final Day19 day19 = new Day19(readStrings(this));
        day19.rules.put(8, "42 | 42 8");
        day19.rules.put(11, "42 31 | 42 11 31");
        assertEquals(282, day19.getValidMessages().size());
    }
}