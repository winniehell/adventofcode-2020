package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day22Test {
    private static final List<String> exampleInput = Arrays.asList("""
            Player 1:
            9
            2
            6
            3
            1
                        
            Player 2:
            5
            8
            4
            7
            10""".stripIndent().split("\n"));

    private static final List<String> exampleInputInfinity = Arrays.asList("""
            Player 1:
            43
            19
                        
            Player 2:
            2
            29
            14""".stripIndent().split("\n"));

    @Test
    void testParseStacks() {
        final Day22.Combat game = new Day22.Combat(exampleInput);
        assertEquals(2, game.players.size());

        final Day22.Player firstPlayer = game.players.get(0);
        assertEquals(1, firstPlayer.id);
        assertEquals(9, firstPlayer.cardStack.peek());
        assertEquals("""
                        Player 1:
                        9
                        2
                        6
                        3
                        1""".stripIndent(),
                firstPlayer.toString()
        );
    }

    @Test
    void testRound() {
        final Day22.Combat game = new Day22.Combat(exampleInput);
        final Day22.Player firstPlayer = game.players.get(0);
        final Day22.Player secondPlayer = game.players.get(1);
        assertEquals(
                List.of(9, 2, 6, 3, 1),
                List.copyOf(firstPlayer.cardStack)
        );
        assertEquals(
                List.of(5, 8, 4, 7, 10),
                List.copyOf(secondPlayer.cardStack)
        );

        game.nextRound();

        assertEquals(
                List.of(2, 6, 3, 1, 9, 5),
                List.copyOf(firstPlayer.cardStack)
        );
        assertEquals(
                List.of(8, 4, 7, 10),
                List.copyOf(secondPlayer.cardStack)
        );

        game.nextRound();

        assertEquals(
                List.of(6, 3, 1, 9, 5),
                List.copyOf(firstPlayer.cardStack)
        );
        assertEquals(
                List.of(4, 7, 10, 8, 2),
                List.copyOf(secondPlayer.cardStack)
        );
    }

    @Test
    void testRunGame() {
        final Day22.Combat game = new Day22.Combat(exampleInput);
        game.run();
        final Day22.Player firstPlayer = game.players.get(0);
        final Day22.Player secondPlayer = game.players.get(1);
        assertEquals(
                Collections.emptyList(),
                List.copyOf(firstPlayer.cardStack)
        );
        assertEquals(
                List.of(3, 2, 10, 6, 8, 5, 9, 4, 7, 1),
                List.copyOf(secondPlayer.cardStack)
        );
    }

    @Test
    void exampleOne() {
        final Day22.Combat game = new Day22.Combat(exampleInput);
        game.run();
        assertEquals(306, game.getWinningScore());
    }

    @Test
    void solutionOne() {
        final Day22.Combat game = new Day22.Combat(readStrings(this));
        game.run();
        assertEquals(34566, game.getWinningScore());
    }

    @Test
    void testRecursiveCombat() {
        final Day22.RecursiveCombat game = new Day22.RecursiveCombat(exampleInput);
        final Day22.Player firstPlayer = game.players.get(0);
        final Day22.Player secondPlayer = game.players.get(1);

        for (int i = 1; i < 9; i++) {
            game.nextRound();
        }

        assertEquals(
                List.of(4, 9, 8, 5, 2),
                List.copyOf(firstPlayer.cardStack)
        );
        assertEquals(
                List.of(3, 10, 1, 7, 6),
                List.copyOf(secondPlayer.cardStack)
        );

        game.nextRound();

        assertEquals(
                List.of(9, 8, 5, 2),
                List.copyOf(firstPlayer.cardStack)
        );
        assertEquals(
                List.of(10, 1, 7, 6, 3, 4),
                List.copyOf(secondPlayer.cardStack)
        );
    }

    @Test
    void testInfinity() {
        final Day22.RecursiveCombat game = new Day22.RecursiveCombat(exampleInputInfinity);
        game.run();
        assertEquals(369, game.getWinningScore());
    }

    @Test
    void solutionTwo() {
        final Day22.RecursiveCombat game = new Day22.RecursiveCombat(readStrings(this));
        game.run();
        assertEquals(31854, game.getWinningScore());
    }
}