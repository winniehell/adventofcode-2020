package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;
import java.util.Set;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day07Test {

    private final List<String> exampleInputOne = List.of("light red bags contain 1 bright white bag, 2 muted yellow bags.",
            "dark orange bags contain 3 bright white bags, 4 muted yellow bags.",
            "bright white bags contain 1 shiny gold bag.",
            "muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.",
            "shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.",
            "dark olive bags contain 3 faded blue bags, 4 dotted black bags.",
            "vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.",
            "faded blue bags contain no other bags.",
            "dotted black bags contain no other bags."
    );

    @Test
    void parseBagContent() {
        assertEquals(new Day07.BagContent(1, "bright white"), Day07.BagContent.parse("1 bright white bag"));
        assertEquals(new Day07.BagContent(2, "muted yellow"), Day07.BagContent.parse("2 muted yellow bags"));
    }

    @Test
    void parseBagRule() {
        assertEquals(
                new Day07.BagRule(
                        "light red",
                        List.of(
                                new Day07.BagContent(1, "bright white"),
                                new Day07.BagContent(2, "muted yellow")
                        )
                ),
                Day07.BagRule.parse("light red bags contain 1 bright white bag, 2 muted yellow bags.")
        );

        assertEquals(
                new Day07.BagRule("dotted black", Collections.emptyList()),
                Day07.BagRule.parse("dotted black bags contain no other bags.")
        );
    }

    @Test
    void exampleOne() {
        final Set<String> expectedContainers = Set.of("bright white", "muted yellow", "dark orange", "light red");
        final Set<String> containers = new Day07(exampleInputOne).getAvailableContainers("shiny gold");
        assertEquals(expectedContainers, containers);
        assertEquals(expectedContainers.size(), new Day07(exampleInputOne).countAvailableContainers("shiny gold"));
    }

    @Test
    void solutionOne() {
        final int containerCount = new Day07(readStrings(this)).countAvailableContainers("shiny gold");
        assertEquals(355, containerCount);
    }

    private final List<String> exampleInputTwo = List.of(
            "shiny gold bags contain 2 dark red bags.",
            "dark red bags contain 2 dark orange bags.",
            "dark orange bags contain 2 dark yellow bags.",
            "dark yellow bags contain 2 dark green bags.",
            "dark green bags contain 2 dark blue bags.",
            "dark blue bags contain 2 dark violet bags.",
            "dark violet bags contain no other bags."
    );

    @Test
    void findRule() {
        assertEquals(
                List.of(Day07.BagContent.parse("2 dark red bags")),
                new Day07(exampleInputTwo).findRule("shiny gold").contents
        );
    }

    @Test
    void exampleTwo() {
        assertEquals(126, new Day07(exampleInputTwo).countMaximumBagsInside("shiny gold"));
    }

    @Test
    void solutionTwo() {
        final long numBagsInside = new Day07(readStrings(this)).countMaximumBagsInside("shiny gold");
        assertEquals(5312, numBagsInside);
    }
}