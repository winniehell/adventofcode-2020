package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day15Test {

    @Test
    void exampleOne() {
        assertEquals(436, new Day15(List.of(0, 3, 6)).runUntil(2020));
        assertEquals(1, new Day15(List.of(1, 3, 2)).runUntil(2020));
        assertEquals(10, new Day15(List.of(2, 1, 3)).runUntil(2020));
        assertEquals(27, new Day15(List.of(1, 2, 3)).runUntil(2020));
        assertEquals(78, new Day15(List.of(2, 3, 1)).runUntil(2020));
        assertEquals(438, new Day15(List.of(3, 2, 1)).runUntil(2020));
        assertEquals(1836, new Day15(List.of(3, 1, 2)).runUntil(2020));
    }

    @Test
    void solutionOne() {
        final List<Integer> numbers = Arrays.stream(readStrings(this).get(0).split(","))
                .map(Integer::valueOf)
                .collect(Collectors.toList());
        assertEquals(1696, new Day15(numbers).runUntil(2020));
    }

    @Test
    void solutionTwo() {
        final List<Integer> numbers = Arrays.stream(readStrings(this).get(0).split(","))
                .map(Integer::valueOf)
                .collect(Collectors.toList());
        assertEquals(37385, new Day15(numbers).runUntil(30000000));
    }
}