package adventofcode2020;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Helper {
    public static BufferedReader getResourceReader(String name) {
        InputStream inputStream = Helper.class.getResourceAsStream(name);
        if (inputStream == null) {
            throw new RuntimeException(new FileNotFoundException("Resource " + name + " does not exist!"));
        }
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        return new BufferedReader(inputStreamReader);
    }

    public static List<String> readStrings(Object testInstance) {
        final Class<?> testClass = testInstance.getClass();
        final String fileName = "/" + testClass.getSimpleName().replace("Test", "") + ".txt";
        try (BufferedReader reader = getResourceReader(fileName)) {
            final ArrayList<String> result = new ArrayList<>();
            while (reader.ready()) {
                String line = reader.readLine();
                result.add(line);
            }
            return result;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static List<Integer> readIntegers(Object testInstance) {
        return readStrings(testInstance)
                .stream()
                .map(Integer::valueOf)
                .collect(Collectors.toList());
    }

    public static List<Long> readLongs(Object testInstance) {
        return readStrings(testInstance)
                .stream()
                .map(Long::valueOf)
                .collect(Collectors.toList());
    }
}
