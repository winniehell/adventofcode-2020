package adventofcode2020;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.junit.jupiter.api.Test;

import java.util.List;

import static adventofcode2020.Helper.readIntegers;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day01Test {

    final List<Integer> exampleInput = List.of(
            1721,
            979,
            366,
            299,
            675,
            1456
    );


    @Test
    public void examplePair() {
        Pair<Integer, Integer> pair = Day01.findPair(exampleInput, 2020);
        assertEquals(Pair.of(1721, 299), pair);
    }

    @Test
    public void exampleTriple() {
        Triple<Integer, Integer, Integer> triple = Day01.findTriple(exampleInput, 2020);
        assertEquals(Triple.of(979,366,675), triple);
    }

    @Test
    public void exampleTripleProduct() {
        long product = Day01.multipliedTriple(exampleInput, 2020);
        assertEquals(241861950, product);
    }

    @Test
    public void solutionPair() {
        final List<Integer> input = readIntegers(this);
        Pair<Integer, Integer> pair = Day01.findPair(input, 2020);
        assertEquals(Pair.of(337,1683), pair);
    }

    @Test
    public void solutionFirst(){
        final List<Integer> inputList = readIntegers(this);
        int product = Day01.multipliedPair(inputList, 2020);
        assertEquals(567171, product);
    }

    @Test
    public void solutionSecond(){
        final List<Integer> inputList = readIntegers(this);
        long product = Day01.multipliedTriple(inputList, 2020);
        assertEquals(212428694, product);
    }
}