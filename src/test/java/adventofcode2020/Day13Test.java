package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Set;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day13Test {

    private final List<String> exampleInput = List.of(
            "939",
            "7,13,x,x,59,x,31,19"
    );

    @Test
    void exampleOne() {
        Day13.Departures departures = new Day13.Departures(exampleInput);
        assertEquals(939, departures.desiredDepartureTime);
        assertEquals(Set.of(7, 13, 59, 31, 19), departures.busLines.keySet());

        assertEquals(59, departures.findBestBus());
        assertEquals(5, departures.getWaitingTime());
    }

    @Test
    void solutionOne() {
        Day13.Departures departures = new Day13.Departures(readStrings(this));
        assertEquals(2406, departures.findBestBus() * departures.getWaitingTime());
    }

    @Test
    void exampleTwo() {
        Day13.Departures departures = new Day13.Departures(exampleInput);
        assertEquals(1068781, departures.findBusOffsetDeparture());
    }

    @Test
    void solutionTwo() {
        Day13.Departures departures = new Day13.Departures(readStrings(this));
        assertEquals(225850756401039L, departures.findBusOffsetDeparture());
    }
}