package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.*;

class Day16Test {
    private static final List<String> exampleInputOne = Arrays.asList("""
            class: 1-3 or 5-7
            row: 6-11 or 33-44
            seat: 13-40 or 45-50
                        
            your ticket:
            7,1,14
                        
            nearby tickets:
            7,3,47
            40,4,50
            55,2,20
            38,6,12""".stripIndent().split("\n"));

    private static final List<String> exampleInputTwo = Arrays.asList("""
            class: 0-1 or 4-19
            row: 0-5 or 8-19
            seat: 0-13 or 16-19
            
            your ticket:
            11,12,13
            
            nearby tickets:
            3,9,18
            15,1,5
            5,14,9""".stripIndent().split("\\n"));
    
    @Test
    void exampleOne() {
        final Day16 day16 = new Day16(exampleInputOne);
        final Supplier<IntStream> firstAttributeRange = day16.attributeRanges.get("class");
        assertEquals(List.of(1, 2, 3, 5, 6, 7), firstAttributeRange.get().boxed().collect(Collectors.toList()));
        assertEquals(Integer.MIN_VALUE, day16.findInvalidValues(day16.nearbyTickets.get(0)).findFirst().orElse(Integer.MIN_VALUE));
        assertEquals(55, day16.findInvalidValues(day16.nearbyTickets.get(2)).findFirst().orElseThrow());
        final int errorRate = day16.findInvalidNearbyTicketValues().sum();
        assertEquals(71, errorRate);
    }

    @Test
    void solutionOne() {
        final int errorRate = new Day16(readStrings(this)).findInvalidNearbyTicketValues().sum();
        assertEquals(20060, errorRate);
    }

    @Test
    void exampleTwo() {
        Day16 day16 = new Day16(exampleInputOne);
        assertEquals(4, day16.nearbyTickets.size());
        day16.discardInvalidTickets();
        assertEquals(1, day16.nearbyTickets.size());

        day16 = new Day16(exampleInputTwo);
        assertEquals(List.of("row", "class", "seat"), day16.findAttributeOrder());
    }

    @Test
    void solutionTwo() {
        final Day16 day16 = new Day16(readStrings(this));
        day16.discardInvalidTickets();
        final List<String> attributeOrder = day16.findAttributeOrder();
        final List<Integer> departureAttributeIndices = IntStream.range(0, attributeOrder.size())
                .filter(i -> attributeOrder.get(i).matches("^departure.*"))
                .boxed()
                .collect(Collectors.toList());
        assertEquals(6, departureAttributeIndices.size());
        final long result = departureAttributeIndices
                .stream()
                .mapToLong(day16.yourTicket::get)
                .reduce(1, (a, b) -> a * b);
        assertEquals(2843534243843L, result);
    }
}