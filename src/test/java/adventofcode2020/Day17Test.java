package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day17Test {

    private static final List<String> exampleInput = Arrays.asList("""
            .#.
            ..#
            ###""".stripIndent().split("\n"));

    @Test
    void exampleOne() {
        final Day17.Grid grid = Day17.Grid.parse(exampleInput);
        assertEquals("z=0\n" + String.join("\n", exampleInput), grid.toString());

        grid.step();

        assertEquals("""
                z=-1
                #..
                ..#
                .#.
                                
                z=0
                #.#
                .##
                .#.
                                
                z=1
                #..
                ..#
                .#.""".stripIndent(), grid.toString());

        grid.step();

        assertEquals("""
                z=-2
                .....
                .....
                ..#..
                .....
                .....
                                
                z=-1
                ..#..
                .#..#
                ....#
                .#...
                .....
                                
                z=0
                ##...
                ##...
                #....
                ....#
                .###.
                                
                z=1
                ..#..
                .#..#
                ....#
                .#...
                .....
                                
                z=2
                .....
                .....
                ..#..
                .....
                .....""".stripIndent(), grid.toString());


        grid.step();
        grid.step();
        grid.step();
        grid.step();

        assertEquals(112, grid.countActiveCells());
    }

    @Test
    void solutionOne() {
        final Day17.Grid grid = Day17.Grid.parse(readStrings(this));
        for (int i = 0; i < 6; i++) {
            grid.step();
        }
        assertEquals(333, grid.countActiveCells());
    }

    @Test
    void exampleTwo() {
        final Day17.Grid grid = Day17.Grid.parse(4, exampleInput);
        for (int i = 0; i < 6; i++) {
            grid.step();
        }
        assertEquals(848, grid.countActiveCells());
    }

    @Test
    void solutionTwo() {
        final Day17.Grid grid = Day17.Grid.parse(4, readStrings(this));
        for (int i = 0; i < 6; i++) {
            grid.step();
        }
        assertEquals(2676, grid.countActiveCells());
    }
}