package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Day23Test {
    @Test
    void testParsing() {
        final Day23.CrabCups game = new Day23.CrabCups("389125467");
        assertEquals(
                List.of(3, 8, 9, 1, 2, 5, 4, 6, 7),
                List.copyOf(game.cups)
        );
        assertEquals(3, game.currentCupValue);
    }

    @Test
    void testMove() {
        final Day23.CrabCups game = new Day23.CrabCups("389125467");
        game.nextMove();
        assertEquals(
                List.of(3, 2, 8, 9, 1, 5, 4, 6, 7),
                List.copyOf(game.cups)
        );
        game.nextMove();
        assertEquals(
                List.of(3, 2, 5, 4, 6, 7, 8, 9, 1),
                List.copyOf(game.cups)
        );
    }

    @Test
    void testExampleOne() {
        final Day23.CrabCups game = new Day23.CrabCups("389125467");
        game.run(10);
        assertEquals(
                List.of(5, 8, 3, 7, 4, 1, 9, 2, 6),
                List.copyOf(game.cups)
        );
    }

    @Test
    void testSolutionOne() {
        final Day23.CrabCups game = new Day23.CrabCups("157623984");
        game.run(100);
        final int startIndex = game.cups.indexOf(1);
        final String result = IntStream.range(startIndex + 1, startIndex + game.cups.size())
                .map(game::wrapIndex)
                .mapToObj(game.cups::get)
                .map(String::valueOf)
                .collect(Collectors.joining());
        assertEquals("58427369", result);
    }
}