package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day05Test {

    private final List<String> exampleInput = List.of(
            "BFFFBBFRRR",
            "FFFBBBFRRR",
            "BBFFBBFRLL"
    );

    @Test
    void exampleOne() {
        assertEquals(List.of(
                new Day05.BoardingPass(70, 7),
                new Day05.BoardingPass(14, 7),
                new Day05.BoardingPass(102, 4)
        ), exampleInput.stream().map(Day05.BoardingPass::parse).collect(Collectors.toList()));
        assertEquals(820, new Day05(exampleInput).highestSeatID());
    }

    @Test
    void solutionOne() {
        final List<String> input = readStrings(this);
        final int highestSeatID = new Day05(input).highestSeatID();
        assertEquals(978, highestSeatID);
    }

    @Test
    void solutionTwo() {
        final List<String> input = readStrings(this);
        final Day05 day05 = new Day05(input);
        final int firstOccupiedSeat = day05.getOccupiedSeats().get(0);
        final List<Integer> emptySeats = day05.getEmptySeats();
        final int mySeatId = emptySeats.stream()
                .filter(i -> i > firstOccupiedSeat)
                .findFirst().orElseThrow();
        assertEquals(727, mySeatId);
    }
}