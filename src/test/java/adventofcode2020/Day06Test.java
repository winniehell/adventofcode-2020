package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.List;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day06Test {

    private final List<String> exampleInput = List.of("abc",
            "",
            "a",
            "b",
            "c",
            "",
            "ab",
            "ac",
            "",
            "a",
            "a",
            "a",
            "a",
            "",
            "b"
    );

    @Test
    void exampleOne() {
        final long numAnswers = new Day06(exampleInput).countUnionAnswers();
        assertEquals(11, numAnswers);
    }

    @Test
    void solutionOne() {
        final long numAnswers = new Day06(readStrings(this)).countUnionAnswers();
        assertEquals(6633, numAnswers);
    }

    @Test
    void exampleTwo() {
        final long numAnswers = new Day06(exampleInput).countIntersectionAnswers();
        assertEquals(6, numAnswers);
    }

    @Test
    void solutionTwo() {
        final long numAnswers = new Day06(readStrings(this)).countIntersectionAnswers();
        assertEquals(3202, numAnswers);
    }
}