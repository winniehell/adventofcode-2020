package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.List;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.*;

class Day08Test {

    private final List<String> exampleInput = List.of(
            "nop +0",
            "acc +1",
            "jmp +4",
            "acc +3",
            "jmp -3",
            "acc -99",
            "acc +1",
            "jmp -4",
            "acc +6"
    );

    @Test
    void exampleOne() {
        final int accumulator = Day08.runUntilLoop(exampleInput);
        assertEquals(5, accumulator);
    }

    @Test
    void solutionOne() {
        final int accumulator = Day08.runUntilLoop(readStrings(this));
        assertEquals(1939, accumulator);
    }

    @Test
    void exampleTwo() {
        final int accumulator = Day08.repairProgram(exampleInput);
        assertEquals(8, accumulator);
    }

    @Test
    void solutionTwo() {
        final int accumulator = Day08.repairProgram(readStrings(this));
        assertEquals(2212, accumulator);
    }
}