package adventofcode2020;

import org.junit.jupiter.api.Test;
import org.locationtech.jts.math.Vector2D;

import java.util.List;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day03Test {

    private final List<String> exampleInput = List.of(
            "..##.......",
            "#...#...#..",
            ".#....#..#.",
            "..#.#...#.#",
            ".#...##..#.",
            "..#.##.....",
            ".#.#.#....#",
            ".#........#",
            "#.##...#...",
            "#...##....#",
            ".#..#...#.#"
    );

    @Test
    void exampleOne() {
        final Vector2D direction = Vector2D.create(3, 1);
        final long trees = new Day03(exampleInput).countTrees(direction);
        assertEquals(7, trees);
    }

    @Test
    void solutionOne() {
        List<String> grid = readStrings(this);
        final Vector2D direction = Vector2D.create(3, 1);
        final long trees = new Day03(grid).countTrees(direction);
        assertEquals(195, trees);
    }

    private final List<Vector2D> directions = List.of(
            Vector2D.create(1, 1),
            Vector2D.create(3, 1),
            Vector2D.create(5, 1),
            Vector2D.create(7, 1),
            Vector2D.create(1, 2)
    );

    @Test
    void exampleTwo() {
        long result = new Day03(exampleInput).countMultipliedTrees(directions);
        assertEquals(336, result);
    }

    @Test
    void solutionTwo() {
        List<String> grid = readStrings(this);
        long result = new Day03(grid).countMultipliedTrees(directions);
        assertEquals(3772314000L, result);
    }
}
