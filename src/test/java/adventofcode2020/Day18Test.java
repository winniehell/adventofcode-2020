package adventofcode2020;

import org.junit.jupiter.api.Test;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Day18Test {
    @Test
    void exampleOne() {
        assertEquals(71, Day18.calculate("1 + 2 * 3 + 4 * 5 + 6"));
        assertEquals(51, Day18.calculate("1 + (2 * 3) + (4 * (5 + 6))"));
        assertEquals(26, Day18.calculate("2 * 3 + (4 * 5)"));
        assertEquals(437, Day18.calculate("5 + (8 * 3 + 9 + 3 * 4 * 3)"));
        assertEquals(12240, Day18.calculate("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))"));
        assertEquals(13632, Day18.calculate("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2"));
    }

    @Test
    void solutionOne() {
        final long result = readStrings(this).stream().mapToLong(Day18::calculate).sum();
        assertEquals(13976444272545L, result);
    }

    @Test
    void exampleTwo() {
        assertEquals(231, Day18.calculate("1 + 2 * 3 + 4 * 5 + 6", true));
        assertEquals(51, Day18.calculate("1 + (2 * 3) + (4 * (5 + 6))", true));
        assertEquals(46, Day18.calculate("2 * 3 + (4 * 5)", true));
        assertEquals(1445, Day18.calculate("5 + (8 * 3 + 9 + 3 * 4 * 3)", true));
        assertEquals(669060, Day18.calculate("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", true));
        assertEquals(23340, Day18.calculate("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", true));
    }

    @Test
    void solutionTwo() {
        final long result = readStrings(this).stream().mapToLong(input -> Day18.calculate(input, true)).sum();
        assertEquals(88500956630893L, result);
    }
}