package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static adventofcode2020.Helper.readStrings;
import static org.junit.jupiter.api.Assertions.*;

class Day14Test {

    private final List<String> exampleInputOne = Arrays.asList("""                
            mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
            mem[8] = 11
            mem[7] = 101
            mem[8] = 0""".stripIndent().split("\n"));

    private final List<String> exampleInputTwo = Arrays.asList("""
            mask = 000000000000000000000000000000X1001X
            mem[42] = 100
            mask = 00000000000000000000000000000000X0XX
            mem[26] = 1""".stripIndent().split("\n"));

    @Test
    void exampleOne() {
        final Day14.Program program = new Day14.Program(exampleInputOne);
        program.run();
        assertEquals(165, program.getValues().sum());
    }

    @Test
    void solutionOne() {
        final Day14.Program program = new Day14.Program(readStrings(this));
        program.run();
        assertEquals(13865835758282L, program.getValues().sum());
    }

    @Test
    void exampleTwo() {
        final Day14.Program program = new Day14.Program(exampleInputTwo, 2);
        program.run();
        assertEquals(208, program.getValues().sum());
    }

    @Test
    void solutionTwo() {
        final Day14.Program program = new Day14.Program(readStrings(this), 2);
        program.run();
        assertEquals(4195339838136L, program.getValues().sum());
    }
}