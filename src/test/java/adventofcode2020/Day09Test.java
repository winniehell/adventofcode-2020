package adventofcode2020;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static adventofcode2020.Helper.readLongs;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class Day09Test {

    private final List<Long> exampleInput = List.of(
            35,
            20,
            15,
            25,
            47,
            40,
            62,
            55,
            65,
            95,
            102,
            117,
            150,
            182,
            127,
            219,
            299,
            277,
            309,
            576
    )
            .stream()
            .mapToLong(Integer::longValue)
            .boxed()
            .collect(Collectors.toList());

    @Test
    void validSequence() {
        assertTrue(Day09.isValidSequence(exampleInput.subList(0, 6)));
    }

    @Test
    void exampleOne() {
        final long invalidNumber = new Day09(exampleInput).firstInvalidNumber(5);
        assertEquals(127, invalidNumber);
    }

    @Test
    void solutionOne() {
        final long invalidNumber = new Day09(readLongs(this)).firstInvalidNumber(25);
        assertEquals(50047984, invalidNumber);
    }

    @Test
    void exampleTwo() {
        final List<Long> contiguousSet = new Day09(exampleInput).findContiguousSet(127);
        assertEquals("[15, 25, 47, 40]", contiguousSet.toString());
    }

    @Test
    void solutionTwo() {
        final List<Long> contiguousSet = new Day09(readLongs(this)).findContiguousSet(50047984);
        final long min = contiguousSet.stream().mapToLong(l -> l).min().orElseThrow();
        final long max = contiguousSet.stream().mapToLong(l -> l).max().orElseThrow();
        assertEquals(5407707, min + max);
    }
}