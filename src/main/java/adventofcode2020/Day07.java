package adventofcode2020;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day07 {
    public static class BagContent {
        public final int quantity;
        public final String bagType;

        public BagContent(int quantity, String bagType) {
            this.quantity = quantity;
            this.bagType = bagType;
        }

        public static final Pattern pattern = Pattern.compile("^(?<quantity>\\d*) (?<bagType>\\w+ \\w+) bags?$");

        public static BagContent parse(String input) {
            final Matcher matcher = pattern.matcher(input);
            if (!matcher.find()) {
                throw new IllegalArgumentException(input + " does not match " + pattern.toString());
            }

            return new BagContent(
                    Integer.parseInt(matcher.group("quantity")),
                    matcher.group("bagType")
            );
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (o == null || getClass() != o.getClass()) return false;

            BagContent that = (BagContent) o;

            return new EqualsBuilder().append(quantity, that.quantity).append(bagType, that.bagType).isEquals();
        }

        @Override
        public int hashCode() {
            return Objects.hash(quantity, bagType);
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("quantity", quantity)
                    .append("bagType", bagType)
                    .toString();
        }
    }

    public static class BagRule {
        public final String bagType;
        public final List<BagContent> contents;

        public BagRule(String bagType, List<BagContent> contents) {
            this.bagType = bagType;
            this.contents = Collections.unmodifiableList(contents);
        }

        private static final Pattern pattern = Pattern.compile("(?<bagType>\\w+ \\w+) bags contain (?<content>.+).");

        public static BagRule parse(String input) {
            final Matcher matcher = pattern.matcher(input);
            if (!matcher.find()) {
                throw new IllegalArgumentException(input + " does not match " + pattern.toString());
            }

            final String contentString = matcher.group("content");

            final List<BagContent> contents;
            if (contentString.equals("no other bags")) {
                contents = Collections.emptyList();
            } else {
                contents = Arrays.stream(contentString.split(", "))
                        .map(BagContent::parse)
                        .collect(Collectors.toList());
            }

            return new BagRule(matcher.group("bagType"), contents);
        }

        public boolean canContain(String bagType) {
            return contents.stream().anyMatch(content -> content.bagType.equals(bagType));
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (o == null || getClass() != o.getClass()) return false;

            BagRule that = (BagRule) o;

            return new EqualsBuilder().append(bagType, that.bagType).append(contents, that.contents).isEquals();
        }

        @Override
        public int hashCode() {
            return Objects.hash(bagType, contents);
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("bagType", bagType)
                    .append("contents", contents)
                    .toString();
        }
    }

    private final List<BagRule> bagRules;

    public Day07(List<String> input) {
        this.bagRules = input.stream()
                .map(BagRule::parse)
                .collect(Collectors.toList());
    }

    public Set<String> getAvailableContainers(String bagType) {
        final HashSet<String> containers = new HashSet<>(List.of(bagType));

        boolean hasNewContainers = true;
        while (hasNewContainers) {
            final List<String> newContainers = containers.stream().map(containerBagType ->
                    this.bagRules.stream()
                            .filter(rule -> rule.canContain(containerBagType))
                            .map(rule -> rule.bagType)
            )
                    .reduce(Stream::concat)
                    .orElseThrow()
                    .collect(Collectors.toList());
            hasNewContainers = containers.addAll(newContainers);
        }

        containers.remove(bagType);

        return containers;
    }

    public int countAvailableContainers(String bagType) {
        return getAvailableContainers(bagType).size();
    }

    public BagRule findRule(String bagType) {
        return this.bagRules.stream()
                .filter(rule -> rule.bagType.equals(bagType))
                .findAny().orElseThrow();
    }

    public long countMaximumBagsInside(String bagType) {
        final BagRule bagRule = findRule(bagType);
        return bagRule.contents.stream()
                .mapToLong(content ->
                        content.quantity * (1 + countMaximumBagsInside(content.bagType))
                )
                .sum();
    }
}
