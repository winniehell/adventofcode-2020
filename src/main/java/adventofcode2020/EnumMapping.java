package adventofcode2020;

import java.util.EnumMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class EnumMapping<E extends Enum<E>> {
    private final EnumMap<E, Character> encoding;
    private final Map<Character, E> lookup;

    public EnumMapping(final Map<E, Character> encoding) {
        encoding.keySet().forEach(Objects::requireNonNull);
        encoding.values().forEach(Objects::requireNonNull);
        this.encoding = new EnumMap<>(encoding);
        this.lookup = encoding.entrySet().stream().collect(Collectors.toMap(Map.Entry::getValue, Map.Entry::getKey));
    }

    public E parse(char value) {
        return Objects.requireNonNull(lookup.get(value));
    }

    public char encode(E value) {
        return Objects.requireNonNull(encoding.get(value));
    }
}
