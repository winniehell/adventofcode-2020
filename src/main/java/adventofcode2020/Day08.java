package adventofcode2020;

import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Day08 {
    public static class Interpreter {
        public int accumulator = 0;
        public int instructionIndex = 0;

        private final List<Pair<Operation, Integer>> instructions;

        public Interpreter(List<String> input) {
            instructions = input.stream().map(Interpreter::parseInstruction).collect(Collectors.toList());
        }

        private static final Pattern instructionPattern = Pattern.compile("^(?<operation>[a-z]+) (?<argument>[+-]\\d+)$");

        public static Pair<Operation, Integer> parseInstruction(String input) {
            final Matcher matcher = instructionPattern.matcher(input);
            if (!matcher.find()) {
                throw new IllegalArgumentException("Invalid instruction: " + input);
            }

            final Operation operation = Operation.valueOf(matcher.group("operation").toUpperCase());
            final Integer argument = Integer.valueOf(matcher.group("argument"));
            return Pair.of(operation, argument);
        }

        boolean step() {
            final Pair<Operation, Integer> instruction = instructions.get(instructionIndex);
            final Operation operation = instruction.getLeft();
            final Integer argument = instruction.getRight();
            operation.execute(this, argument);
            return 0 <= instructionIndex && instructionIndex < instructions.size();
        }
    }

    public enum Operation {
        ACC {
            @Override
            public void execute(Interpreter interpreter, int argument) {
                interpreter.accumulator += argument;
                interpreter.instructionIndex += 1;
            }
        },

        JMP {
            @Override
            public void execute(Interpreter interpreter, int argument) {
                interpreter.instructionIndex += argument;
            }
        },

        NOP {
            @Override
            public void execute(Interpreter interpreter, int argument) {
                interpreter.instructionIndex += 1;
            }
        };

        abstract void execute(Interpreter interpreter, int argument);
    }

    public static int runUntilLoop(List<String> input) {
        final Interpreter interpreter = new Interpreter(input);
        findLoop(interpreter);
        return interpreter.accumulator;
    }

    private static boolean findLoop(Interpreter interpreter) {
        final List<Integer> visitedInstructions = new ArrayList<>();

        while (!visitedInstructions.contains(interpreter.instructionIndex)) {
            visitedInstructions.add(interpreter.instructionIndex);

            if (!interpreter.step()) {
                return false;
            }
        }

        return true;
    }

    public static int repairProgram(List<String> input) {
        for (int patchIndex = 0; patchIndex < input.size(); patchIndex++) {
            final Interpreter interpreter = new Interpreter(input);

            final Pair<Operation, Integer> instruction = interpreter.instructions.get(patchIndex);
            Operation operation = instruction.getLeft();

            if (operation == Operation.ACC) {
                continue;
            }

            operation = switch (operation) {
                case JMP -> Operation.NOP;
                case NOP -> Operation.JMP;
                default -> operation;
            };

            final Integer argument = instruction.getRight();
            interpreter.instructions.set(patchIndex, Pair.of(operation, argument));

            if (!findLoop(interpreter)) {
                return interpreter.accumulator;
            }
        }

        throw new UnsupportedOperationException("I'm sorry Dave, I'm afraid I can't repair that.");
    }
}
