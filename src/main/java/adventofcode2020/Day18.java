package adventofcode2020;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day18 {

    private static final Pattern additionPattern = Pattern.compile("^(?<left>([^+]*[^\\d])?)(?<first>\\d+) [+] (?<second>\\d+)(?<right>.*)$");
    private static final Pattern numberPattern = Pattern.compile("^\\d+$");
    private static final Pattern operationPattern = Pattern.compile("^(?<rest>.*) (?<operation>[*+]) (?<operand>\\d+)$");
    private static final Pattern parenthesesPattern = Pattern.compile("(?<left>.*)\\((?<inside>[^()]+)\\)(?<right>.*)$");

    public static long calculate(String input) {
        return calculate(input, false);
    }

    public static long calculate(String input, boolean additionPrecedence) {
        if (numberPattern.matcher(input).matches()) {
            return Integer.parseInt(input);
        }

        final Matcher parenthesesMatcher = parenthesesPattern.matcher(input);
        if (parenthesesMatcher.find()) {
            return calculate(
                    parenthesesMatcher.group("left")
                            + calculate(parenthesesMatcher.group("inside"), additionPrecedence)
                            + parenthesesMatcher.group("right"),
                    additionPrecedence
            );
        }

        if (additionPrecedence) {
            final Matcher matcher = additionPattern.matcher(input);
            if (matcher.find()) {
                return calculate(
                        matcher.group("left")
                                + (Integer.parseInt(matcher.group("first")) + Integer.parseInt(matcher.group("second")))
                                + matcher.group("right"),
                        additionPrecedence
                );
            }
        }

        final Matcher operationMatcher = operationPattern.matcher(input);
        if (!operationMatcher.find()) {
            throw new IllegalArgumentException("Invalid input: " + input);
        }
        final int operand = Integer.parseInt(operationMatcher.group("operand"));
        final long rest = calculate(operationMatcher.group("rest"), additionPrecedence);

        final char operation = operationMatcher.group("operation").charAt(0);
        return switch (operation) {
            case '+' -> operand + rest;
            case '*' -> operand * rest;
            default -> throw new IllegalArgumentException("Invalid operation:" + operation);
        };
    }
}
