package adventofcode2020;

import org.locationtech.jts.math.Vector2D;

import java.util.List;
import java.util.stream.LongStream;

public class Day03 {

    private final List<String> grid;

    public Day03(List<String> grid) {
        this.grid = grid;
    }

    public long countTrees(Vector2D directionVector) {
        return LongStream.range(0, Math.round(this.grid.size() / directionVector.getY()))
                .mapToObj(directionVector::multiply)
                .filter(this::isTree)
                .count();
    }

    public long countMultipliedTrees(List<Vector2D> directionVectors) {
        return directionVectors.stream()
                .mapToLong(this::countTrees)
                .reduce(1, (a, b) -> a * b);
    }

    private boolean isTree(Vector2D coordinate) {
        String row = grid.get((int) coordinate.getY());
        char cell = row.charAt((int) coordinate.getX() % row.length());

        return switch (cell) {
            case '#' -> true;
            default -> false;
        };
    }
}
