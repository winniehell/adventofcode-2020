package adventofcode2020;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.LongStream;

public class Day14 {
    public static class Program {
        private final List<String> instructions;
        private final Map<Long, Long> memory = new HashMap<>();
        private final int version;
        private String currentMask;

        private static final Pattern maskPattern = Pattern.compile("^mask = (?<value>[X01]{36})$");
        private static final Pattern memoryPattern = Pattern.compile("^mem\\[(?<address>[0-9]+)] = (?<value>[0-9]+)$");

        public Program(List<String> instructions, int version) {
            this.instructions = instructions;
            this.version = version;
        }

        public Program(List<String> instructions) {
            this(instructions, 1);
        }

        public void run() {
            instructions.forEach(line -> {
                Matcher matcher = maskPattern.matcher(line);
                if (matcher.find()) {
                    currentMask = matcher.group("value");
                    return;
                }

                matcher = memoryPattern.matcher(line);
                if (!matcher.find()) {
                    throw new IllegalArgumentException("Unknown instruction: " + line);
                }

                long address = Long.parseLong(matcher.group("address"));
                long value = Long.parseLong(matcher.group("value"));
                if (version == 2) {
                    applyAddressMask(address).forEach(a -> memory.put(a, value));
                } else {
                    memory.put(address, applyValueMask(value));
                }
            });
        }

        private List<Long> applyAddressMask(long address) {
            if (!currentMask.contains("X")) {
                return Collections.singletonList(address | Long.parseLong(currentMask));
            }

            final List<Integer> floatingBits = IntStream.range(0, currentMask.length())
                    .filter(i -> currentMask.charAt(i) == 'X')
                    .map(i -> currentMask.length() - 1 - i)
                    .boxed()
                    .collect(Collectors.toList());

            final int numAddresses = 1 << floatingBits.size();
            final List<Long> addresses = new ArrayList<>();
            for (int i = 0; i < numAddresses; i++) {
                final BitSet bitSet = BitSet.valueOf(new long[]{i});
                long maskedAddress = address | Long.parseLong(currentMask.replaceAll("X", "0"), 2);
                for (int b = 0; b < floatingBits.size(); b++) {
                    if (bitSet.get(b)) {
                        maskedAddress |= (1L << floatingBits.get(b));
                    } else {
                        maskedAddress &= ~(1L << floatingBits.get(b));
                    }
                }
                addresses.add(maskedAddress);
            }

            return addresses;
        }

        private long applyValueMask(long value) {
            final long andMask = Long.parseLong(currentMask.replaceAll("X", "1"), 2);
            final long orMask = Long.parseLong(currentMask.replaceAll("X", "0"), 2);
            return (value & andMask) | orMask;
        }

        public LongStream getValues() {
            return memory.values().stream().mapToLong(l -> l);
        }
    }
}
