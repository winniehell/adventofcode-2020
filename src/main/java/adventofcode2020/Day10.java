package adventofcode2020;

import java.util.HashMap;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day10 {
    private final List<Integer> sortedJoltages;

    public Day10(List<Integer> input) {
        sortedJoltages = input.stream().sorted().collect(Collectors.toList());

        final int highestJoltage = sortedJoltages.get(sortedJoltages.size() - 1);
        this.sortedJoltages.add(highestJoltage + 3);
    }

    public List<Integer> getJoltageDifferences() {
        return IntStream.range(0, sortedJoltages.size())
                .map(index -> {
                    if (index == 0) {
                        return sortedJoltages.get(index);
                    }
                    return sortedJoltages.get(index) - sortedJoltages.get(index - 1);
                })
                .boxed()
                .collect(Collectors.toList());
    }

    public SortedMap<Integer, Integer> getJoltageDifferenceMap() {
        final TreeMap<Integer, Integer> map = new TreeMap<>();
        getJoltageDifferences().forEach(difference ->
                map.put(difference, map.getOrDefault(difference, 0) + 1)
        );
        return map;
    }

    public long countPossibleArrangements() {
        final HashMap<Integer, Long> possibleArrangements = new HashMap<>();
        possibleArrangements.put(0, 1L);
        sortedJoltages.forEach(joltage -> possibleArrangements.put(joltage,
                possibleArrangements.getOrDefault(joltage - 1, 0L) +
                        possibleArrangements.getOrDefault(joltage - 2, 0L) +
                        possibleArrangements.getOrDefault(joltage - 3, 0L)
        ));
        return possibleArrangements.get(sortedJoltages.get(sortedJoltages.size() - 1));
    }
}
