package adventofcode2020;

import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static adventofcode2020.Utils.*;

public class Day20 {

    private static final char WAVE = '#';

    enum Side {
        BOTTOM,
        LEFT,
        RIGHT,
        TOP;

        public Side opposite() {
            return switch (this) {
                case BOTTOM -> TOP;
                case LEFT -> RIGHT;
                case RIGHT -> LEFT;
                case TOP -> BOTTOM;
            };
        }
    }

    static class Tile {
        static final Pattern headerPattern = Pattern.compile("Tile (?<id>\\d+):");

        final int id;
        List<String> grid;
        EnumMap<Side, String> edges;

        Tile(int id, List<String> grid) {
            this.id = id;
            this.grid = grid;
            updateEdges();
        }

        void updateEdges() {
            this.edges = new EnumMap<>(Map.of(
                    Side.TOP, grid.get(0),
                    Side.LEFT, getColumn(grid, 0),
                    Side.RIGHT, getColumn(grid, grid.get(0).length() - 1),
                    Side.BOTTOM, grid.get(grid.size() - 1)
            ));
        }

        static Tile parse(List<String> input) {
            Matcher header = headerPattern.matcher(input.get(0));
            if (!header.matches()) {
                throw new IllegalArgumentException("Missing header in tile: " + String.join("\n", input));
            }

            int id = Integer.parseInt(header.group("id"));
            return new Tile(id, input.subList(1, input.size()));
        }

        @Override
        public String toString() {
            return headerPattern.pattern().replace("(?<id>\\d+)", String.valueOf(id)) + "\n"
                    + String.join("\n", grid);
        }

        public void transformToMatch(Side side, String edge) {
            if (edges.get(side).equals(edge)) {
                return;
            }

            for (int i = 0; i < 3; i++) {
                rotate();
                if (edges.get(side).equals(edge)) {
                    return;
                }
            }

            flip();
            if (edges.get(side).equals(edge)) {
                return;
            }

            for (int i = 0; i < 3; i++) {
                rotate();
                if (edges.get(side).equals(edge)) {
                    return;
                }
            }

            throw new RuntimeException("Could not find transform... :-(");
        }

        private void rotate() {
            grid = rotateGrid(grid);
            updateEdges();
        }

        private void flip() {
            grid = reverseList(grid);
            updateEdges();
        }

        List<String> withoutEdges() {
            return IntStream.range(1, grid.size() - 1)
                    .mapToObj(grid::get)
                    .map(line -> line.substring(1, line.length() - 1))
                    .collect(Collectors.toList());
        }

        public boolean hasEdge(String edge) {
            return edges.containsValue(edge);
        }

        public Map.Entry<Side, String> intersect(Tile other) {
            return edges.entrySet().stream()
                    .filter(entry -> {
                        String edge = entry.getValue();
                        String flippedEdge = reverse(edge);
                        return other.hasEdge(edge) || other.hasEdge(flippedEdge);
                    })
                    .findAny()
                    .orElseThrow();
        }
    }

    static class ArrangedTiles {
        final Map<Pair<Integer, Integer>, Tile> tiles;

        ArrangedTiles(ImageTiles imageTiles) {
            this.tiles = new HashMap<>();

            Tile topLeftCorner = imageTiles.getCornerTiles().stream()
                    .map(imageTiles.tileMap::get)
                    .filter(tile -> {
                        Set<Tile> neighbors = imageTiles.neighborMap.get(tile.id);
                        for (int i = 0; i < 4; i++) {
                            Set<Side> neighborSides = neighbors.stream()
                                    .map(tile::intersect)
                                    .map(Map.Entry::getKey)
                                    .collect(Collectors.toSet());
                            if (neighborSides.equals(Set.of(
                                    Side.BOTTOM, Side.RIGHT
                            ))) {
                                return true;
                            }
                            tile.rotate();
                        }
                        return false;
                    })
                    .findAny()
                    .orElseThrow();

            Pair<Integer, Integer> origin = Pair.of(0, 0);
            tiles.put(origin, topLeftCorner);

            Deque<Pair<Integer, Integer>> tileQueue = new ArrayDeque<>();
            tileQueue.addLast(origin);

            while (!tileQueue.isEmpty()) {
                Pair<Integer, Integer> tilePosition = tileQueue.poll();
                Tile tile = tiles.get(tilePosition);
                Set<Tile> neighbors = imageTiles.neighborMap.get(tile.id);

                neighbors.forEach(neighbor -> {
                    Map.Entry<Side, String> intersection = tile.intersect(neighbor);
                    Side neighborSide = intersection.getKey();
                    String edge = intersection.getValue();

                    Pair<Integer, Integer> neighborPosition;
                    if (neighborSide == Side.RIGHT) {
                        neighborPosition = Pair.of(
                                tilePosition.getLeft() + 1,
                                tilePosition.getRight()
                        );
                    } else if (neighborSide == Side.BOTTOM) {
                        neighborPosition = Pair.of(
                                tilePosition.getLeft(),
                                tilePosition.getRight() + 1
                        );
                    } else {
                        return;
                    }

                    if (tiles.containsKey(neighborPosition)) {
                        return;
                    }

                    neighbor.transformToMatch(neighborSide.opposite(), edge);
                    tiles.put(neighborPosition, neighbor);
                    tileQueue.addLast(neighborPosition);
                });

                if (tiles.size() >= imageTiles.tileMap.size()) {
                    break;
                }
            }
        }

        CombinedImage combine() {
            return new CombinedImage(this);
        }

        @Override
        public String toString() {
            Pair<Integer, Integer> max = Collections.max(tiles.keySet());
            return IntStream.rangeClosed(0, max.getRight())
                    .mapToObj(y -> IntStream.rangeClosed(0, max.getLeft())
                            .mapToObj(x -> Pair.of(x, y))
                            .map(position -> position + ":" + tiles.get(position).id)
                            .collect(Collectors.joining(" "))
                    )
                    .collect(Collectors.joining("\n"));
        }
    }

    static class CombinedImage {
        List<String> grid;

        CombinedImage(ArrangedTiles arrangedTiles) {
            Pair<Integer, Integer> max = Collections.max(arrangedTiles.tiles.keySet());
            List<List<List<String>>> tilesWithoutEdges = IntStream.rangeClosed(0, max.getRight())
                    .mapToObj(y -> IntStream.rangeClosed(0, max.getLeft())
                            .mapToObj(x -> Pair.of(x, y))
                            .map(arrangedTiles.tiles::get)
                            .map(Tile::withoutEdges)
                            .collect(Collectors.toList())
                    )
                    .collect(Collectors.toList());

            grid = new ArrayList<>();
            tilesWithoutEdges.forEach(list -> grid.addAll(concatenateRows(list)));
        }

        void rotate() {
            grid = rotateGrid(grid);
        }

        void flip() {
            grid = reverseList(grid);
        }

        long countSeaMonsters() {
            return IntStream.range(0, grid.size())
                    .mapToLong(y ->
                            IntStream.range(0, grid.get(y).length())
                                    .filter(x -> hasSeaMonster(x, y))
                                    .count()
                    )
                    .sum();
        }

        boolean hasSeaMonster(int baseX, int baseY) {
            return SeaMonster.getPixelPositions()
                    .allMatch(p -> {
                        int y = baseY + p.getRight();
                        if (y >= grid.size()) {
                            return false;
                        }

                        String row = grid.get(y);
                        int x = baseX + p.getLeft();
                        if (x >= row.length()) {
                            return false;
                        }

                        return row.charAt(x) == WAVE;
                    });
        }

        long calculateWaterRoughness() {
            {
                long count = countSeaMonsters();
                if (count > 0) {
                    return countWaves() - SeaMonster.size() * count;
                }
            }

            for (int i = 0; i < 3; i++) {
                rotate();
                long count = countSeaMonsters();
                if (count > 0) {
                    return countWaves() - SeaMonster.size() * count;
                }
            }

            {
                flip();
                long count = countSeaMonsters();
                if (count > 0) {
                    return countWaves() - SeaMonster.size() * count;
                }
            }

            for (int i = 0; i < 3; i++) {
                rotate();
                long count = countSeaMonsters();
                if (count > 0) {
                    return countWaves() - SeaMonster.size() * count;
                }
            }

            throw new UnsupportedOperationException("what?! no monsters found!");
        }

        long countWaves() {
            return grid.stream()
                    .mapToLong(row -> row.chars()
                            .filter(c -> c == WAVE)
                            .count()
                    )
                    .sum();
        }
    }

    static class ImageTiles {
        final Map<Integer, Tile> tileMap;
        final Map<Integer, Set<Tile>> neighborMap;

        public ImageTiles(List<String> input) {
            tileMap = splitByBlankLine(input).stream()
                    .map(Tile::parse)
                    .map(tile -> Pair.of(tile.id, tile))
                    .collect(Collectors.toMap(Pair::getKey, Pair::getValue));

            neighborMap = tileMap.entrySet().stream()
                    .map(entry -> Pair.of(
                            entry.getKey(),
                            findNeighbors(entry.getValue()))
                    )
                    .collect(Collectors.toMap(Pair::getKey, Pair::getValue));
        }

        Set<Tile> findNeighbors(Tile tile) {
            return tile.edges.values().stream()
                    .map(edge -> findByEdge(edge).filter(t -> t != tile))
                    .reduce(Stream::concat)
                    .orElseThrow()
                    .collect(Collectors.toSet());
        }

        Stream<Tile> findByEdge(String edge) {
            String flippedEdge = reverse(edge);
            return tileMap.values().stream()
                    .filter(tile -> {
                        Collection<String> edges = tile.edges.values();
                        return edges.contains(edge) || edges.contains(flippedEdge);
                    });
        }

        Set<Integer> getCornerTiles() {
            return neighborMap.entrySet().stream()
                    .filter(entry -> entry.getValue().size() == 2)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toSet());
        }

        ArrangedTiles arrange() {
            return new ArrangedTiles(this);
        }
    }

    static class SeaMonster {
        static String pattern = """
                                  #
                #    ##    ##    ###
                 #  #  #  #  #  #""".stripIndent();

        static Stream<Pair<Integer, Integer>> getPixelPositions() {
            List<String> lines = Arrays.asList(pattern.split("\n"));
            return IntStream.range(0, lines.size())
                    .mapToObj(y -> {
                        String line = lines.get(y);
                        return IntStream.range(0, line.length())
                                .filter(x -> line.charAt(x) == WAVE)
                                .mapToObj(x -> Pair.of(x, y));
                    })
                    .reduce(Stream::concat)
                    .orElseThrow();
        }

        static long size() {
            return pattern.chars().filter(c -> c == WAVE).count();
        }
    }
}
