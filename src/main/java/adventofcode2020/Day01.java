package adventofcode2020;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;

import java.util.List;

public class Day01 {
    public static Pair<Integer, Integer> findPair(List<Integer> inputNumbers, int sum) {

        for (int i = 0; i < inputNumbers.size(); i++) {

            for (int j = i + 1; j < inputNumbers.size(); j++) {

                if (inputNumbers.get(i) + inputNumbers.get(j) == sum) {
                    return Pair.of(inputNumbers.get(i), inputNumbers.get(j));
                }
            }
        }
        throw new IllegalArgumentException("no solution found :(");
    }

    public static int multipliedPair(List<Integer> inputList, int sum) {
        Pair<Integer, Integer> pair = findPair(inputList, sum);
        return pair.getLeft() * pair.getRight();
    }

    public static Triple<Integer, Integer, Integer> findTriple(List<Integer> inputList, int sum) {

        for (int i = 0; i < inputList.size(); i++) {
            for (int j = i + 1; j < inputList.size(); j++) {
                for (int k = j + 1; k < inputList.size(); k++) {
                    if (inputList.get(i) + inputList.get(j) + inputList.get(k) == sum) {
                        return Triple.of(inputList.get(i), inputList.get(j), inputList.get(k));
                    }
                }
            }
        }
        throw new IllegalArgumentException("no solution found :(");


    }

    public static long multipliedTriple(List<Integer> inputList, int sum) {
        Triple<Integer, Integer, Integer> triple = findTriple(inputList, sum);
        long product = triple.getLeft().longValue() * triple.getMiddle().longValue() * triple.getRight().longValue();
        return product;
    }
}
