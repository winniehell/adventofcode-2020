package adventofcode2020;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Day12 {
    public static class Position {
        long x;
        long y;

        public Position(long x, long y) {
            this.x = x;
            this.y = y;
        }

        public long getManhattanDistance() {
            return Math.abs(x) + Math.abs(y);
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("x", x)
                    .append("y", y)
                    .toString();
        }
    }

    public enum Direction implements Instruction<Position> {
        EAST(90) {
            @Override
            public void apply(Position position, int argument) {
                position.x += argument;
            }
        },
        NORTH(0)  {
            @Override
            public void apply(Position position, int argument) {
                position.y += argument;
            }
        },
        SOUTH(180)  {
            @Override
            public void apply(Position position, int argument) {
                position.y -= argument;
            }
        },
        WEST(270)  {
            @Override
            public void apply(Position position, int argument) {
                position.x -= argument;
            }
        };

        private final int angle;

        Direction(int angle) {
            this.angle = angle;
        }

        public Direction applyAngle(int angleDifference) {
            int newAngle = (this.angle + angleDifference) % 360;
            while (newAngle < 0) {
                newAngle += 360;
            }
            return findByAngle(newAngle);
        }

        public Direction findByAngle(int angle) {
            return Arrays.stream(values()).filter(v -> v.angle == angle).findAny().orElseThrow();
        }
    }

    public static class Waypoint {
        Position position = new Position(10, 1);

        void rotate(int angle) {
            if (angle == 0) {
                return;
            }

            if (angle < 0) {
                rotate(360 + angle);
                return;
            }

            // noinspection SuspiciousNameCombination
            position = new Position(position.y, -position.x);
            rotate(angle - 90);
        }
    }

    public static class Ship {
        Position position = new Position(0, 0);
        Direction direction = Direction.EAST;
        Waypoint waypoint = new Waypoint();
    }

    public interface Instruction<T> {
        void apply(T target, int argument);
    }

    public enum ShipInstruction implements Instruction<Ship> {
        EAST('E', Direction.EAST),
        FORWARD('F') {
            @Override
            public void apply(Ship ship, int argument) {
                ship.direction.apply(ship.position, argument);
            }
        },
        LEFT('L') {
            @Override
            public void apply(Ship ship, int argument) {
                ship.direction = ship.direction.applyAngle(-argument);
            }
        },
        NORTH('N', Direction.NORTH),
        RIGHT('R') {
            @Override
            public void apply(Ship ship, int argument) {
                ship.direction = ship.direction.applyAngle(argument);
            }
        },
        SOUTH('S', Direction.SOUTH),
        WEST('W', Direction.WEST);

        private final char encoded;
        private final Instruction<Position> positionInstruction;

        ShipInstruction(char encoded, Instruction<Position> positionInstruction) {
            this.encoded = encoded;
            this.positionInstruction = positionInstruction;
        }

        ShipInstruction(char encoded) {
            this(encoded, null);
        }

        public static ShipInstruction findByChar(char encoded) {
            return Arrays.stream(values()).filter(v -> v.encoded == encoded).findAny().orElseThrow();
        }

        @Override
        public void apply(Ship ship, int argument) {
            positionInstruction.apply(ship.position, argument);
        }
    }

    public enum WaypointInstruction implements Instruction<Ship> {
        EAST('E', Direction.EAST),
        FORWARD('F') {
            @Override
            public void apply(Ship ship, int argument) {
                ship.position.x += ship.waypoint.position.x * argument;
                ship.position.y += ship.waypoint.position.y * argument;
            }
        },
        LEFT('L') {
            @Override
            public void apply(Ship ship, int argument) {
                ship.waypoint.rotate(-argument);
            }
        },
        NORTH('N', Direction.NORTH),
        RIGHT('R') {
            @Override
            public void apply(Ship ship, int argument) {
                ship.waypoint.rotate(argument);
            }
        },
        SOUTH('S', Direction.SOUTH),
        WEST('W', Direction.WEST);

        private final char encoded;
        private final Instruction<Position> positionInstruction;

        WaypointInstruction(char encoded, Instruction<Position> positionInstruction) {
            this.encoded = encoded;
            this.positionInstruction = positionInstruction;
        }

        WaypointInstruction(char encoded) {
            this(encoded, null);
        }

        public static WaypointInstruction findByChar(char encoded) {
            return Arrays.stream(values()).filter(v -> v.encoded == encoded).findAny().orElseThrow();
        }

        @Override
        public void apply(Ship ship, int argument) {
            positionInstruction.apply(ship.waypoint.position, argument);
        }
    }

    public static class Navigation {
        private final boolean useWaypoints;
        private final List<Pair<Instruction<Ship>, Integer>> instructions;

        public Navigation(List<String> input, boolean useWaypoints) {
            this.useWaypoints = useWaypoints;
            this.instructions = input.stream()
                    .map(this::parseInstruction)
                    .collect(Collectors.toList());
        }

        public Navigation(List<String> input) {
            this(input, false);
        }

        public Pair<Instruction<Ship>, Integer> parseInstruction(String input) {
            Instruction<Ship> instruction;

            if (useWaypoints) {
                instruction = WaypointInstruction.findByChar(input.charAt(0));
            } else {
                instruction = ShipInstruction.findByChar(input.charAt(0));
            }

            Integer argument = Integer.valueOf(input.substring(1));
            return Pair.of(instruction, argument);
        }

        public Ship run() {
            final Ship ship = new Ship();
            instructions.forEach(pair -> {
                final Instruction<Ship> instruction = pair.getKey();
                final Integer argument = pair.getValue();
                instruction.apply(ship, argument);
            });
            return ship;
        }
    }
}
