package adventofcode2020;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Day11 {
    public enum Cell {
        EMPTY('L'),
        FLOOR('.'),
        OCCUPIED('#');

        private final char encoded;

        Cell(char encoded) {
            this.encoded = encoded;
        }

        public static Cell parse(int encoded) {
            return Arrays.stream(values())
                    .filter(value -> value.encoded == encoded)
                    .findAny()
                    .orElseThrow();
        }
    }

    public static class GridCoordinate {
        public final int row;
        public final int column;

        public GridCoordinate(int row, int column) {
            this.row = row;
            this.column = column;
        }
    }

    public enum NeighborMode {
        ADJACENT,
        LINE_OF_SIGHT
    }

    public static class Grid {

        private NeighborMode neighborMode = NeighborMode.ADJACENT;
        private List<List<Cell>> cells;
        private int acceptableNeighbors = 4;

        public Grid(List<List<Cell>> cells) {
            this.cells = cells;
        }

        public Grid setNeighborMode(NeighborMode neighborMode) {
            this.neighborMode = neighborMode;
            return this;
        }

        public Grid setAcceptableNeighbors(int acceptableNeighbors) {
            this.acceptableNeighbors = acceptableNeighbors;
            return this;
        }

        public void step() {
            this.cells = getAllCoordinates()
                    .map(rowStream -> rowStream.map(this::getNextState).collect(Collectors.toList()))
                    .collect(Collectors.toList());
        }

        private Stream<Stream<GridCoordinate>> getAllCoordinates() {
            return IntStream.range(0, cells.size())
                    .mapToObj(this::getColumnCoordinates);
        }

        private Stream<GridCoordinate> getColumnCoordinates(int rowIndex) {
            final List<Cell> row = this.cells.get(rowIndex);
            return IntStream.range(0, row.size())
                    .mapToObj(columnIndex -> new GridCoordinate(rowIndex, columnIndex));
        }

        private Cell getNextState(GridCoordinate position) {
            final Cell currentState = cells.get(position.row).get(position.column);
            return switch (currentState) {
                case EMPTY -> getOccupiedNeighbors(position) == 0 ? Cell.OCCUPIED : currentState;
                case OCCUPIED -> getOccupiedNeighbors(position) >= acceptableNeighbors ? Cell.EMPTY : currentState;
                default -> currentState;
            };
        }

        private int getOccupiedNeighbors(GridCoordinate position) {
            return (int) getNeighbors(position)
                    .filter(cell -> cell == Cell.OCCUPIED)
                    .count();
        }

        private Stream<GridCoordinate> getNeighborCoordinates(GridCoordinate position) {
            final Stream<GridCoordinate> adjacentNeighbors =
                    IntStream.rangeClosed(position.row - 1, position.row + 1)
                            .mapToObj(row ->
                                    IntStream.rangeClosed(position.column - 1, position.column + 1)
                                            .filter(column -> row != position.row || column != position.column)
                                            .mapToObj(column -> new GridCoordinate(row, column))
                            )
                            .reduce(Stream::concat)
                            .orElseThrow();

            return switch (neighborMode) {
                case ADJACENT -> adjacentNeighbors;
                case LINE_OF_SIGHT -> adjacentNeighbors.map(coordinate -> shootRay(position, coordinate));
            };
        }

        private GridCoordinate shootRay(GridCoordinate start, GridCoordinate direction) {
            GridCoordinate target = direction;
            while (getCell(target) == Cell.FLOOR) {
                target = new GridCoordinate(
                        target.row + direction.row - start.row,
                        target.column + direction.column - start.column
                );
            }
            return target;
        }

        private Cell getCell(GridCoordinate position) {
            if (position.row < 0 || position.row >= cells.size()) {
                return null;
            }

            final List<Cell> row = this.cells.get(position.row);
            if (position.column < 0 || position.column >= row.size()) {
                return null;
            }

            return row.get(position.column);
        }

        private Stream<Cell> getNeighbors(GridCoordinate position) {
            return getNeighborCoordinates(position).map(this::getCell);
        }

        public long countOccupiedSeats() {
            return cells.stream()
                    .mapToLong(row -> row.stream().mapToLong(cell -> cell == Cell.OCCUPIED ? 1 : 0).sum())
                    .sum();
        }

        public static Grid parse(List<String> input) {
            return new Grid(
                    input.stream()
                            .map(Grid::parseRow)
                            .collect(Collectors.toList())
            );
        }

        private static List<Cell> parseRow(String line) {
            return line.chars().mapToObj(Cell::parse).collect(Collectors.toList());
        }

        @Override
        public String toString() {
            return cells.stream()
                    .map(row ->
                            row.stream()
                                    .map(cell -> String.valueOf(cell.encoded))
                                    .collect(Collectors.joining(""))
                    )
                    .collect(Collectors.joining("\n"));
        }
    }

    public static Grid getStabilizedGrid(Grid grid) {
        String oldState = "";
        String state = grid.toString();
        while (!state.equals(oldState)) {
            oldState = state;
            grid.step();
            state = grid.toString();
        }
        return grid;
    }
}
