package adventofcode2020;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Day09 {

    private final List<Long> numbers;

    public Day09(List<Long> input) {
        numbers = input;
    }

    long firstInvalidNumber(int preambleLength) {
        final int sequenceLength = preambleLength + 1;
        return IntStream.range(0, numbers.size())
                .filter(startIndex -> !Day09.isValidSequence(
                        numbers.subList(startIndex, startIndex + sequenceLength)
                ))
                .mapToLong(startIndex -> numbers.get(startIndex + sequenceLength - 1))
                .findFirst()
                .orElseThrow();
    }

    public static boolean isValidSequence(List<Long> sequence) {
        final long lastNumber = sequence.get(sequence.size() - 1);
        return sequence.stream()
                .limit(sequence.size() - 1)
                .filter(number -> 2 * number != lastNumber)
                .anyMatch(summand -> sequence.contains(lastNumber - summand));
    }

    public static boolean isContiguousSet(List<Long> sequence, long sum) {
        return sequence.stream().mapToLong(l -> l).sum() == sum;
    }

    public List<Long> findContiguousSet(long sum) {
        return IntStream.range(0, numbers.size())
                .mapToObj(startIndex ->
                        IntStream.range(startIndex + 2, numbers.size())
                                .mapToObj(toIndex -> numbers.subList(startIndex, toIndex))
                )
                .reduce(Stream::concat)
                .orElseThrow()
                .filter(sequence -> isContiguousSet(sequence, sum))
                .findFirst()
                .orElseThrow();
    }
}
