package adventofcode2020;

import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static adventofcode2020.Utils.reverseList;
import static adventofcode2020.Utils.splitByBlankLine;

public class Day19 {

    final Map<Integer, String> rules;
    final List<String> messages;

    private static final Pattern rulePattern = Pattern.compile("(?<index>\\d+): (?<conditions>.*)");
    private static final Pattern constantPattern = Pattern.compile("\"(?<value>[^\"]*)\"");
    private static final Pattern unionPattern = Pattern.compile("(?<left>[^|]+) \\| (?<right>.+)");
    private static final Pattern concatenationPattern = Pattern.compile("(\\d+)( \\d+)*");

    static class InterpreterContext {
        final String input;
        final int maxBranches;
        final Deque<String> pendingRules;
        int position;
        final ArrayList<Object> appliedRules;

        InterpreterContext(String input, int maxBranches) {
            this.input = input;
            this.maxBranches = maxBranches;
            this.pendingRules = new ArrayDeque<>();
            this.appliedRules = new ArrayList<>();
        }

        InterpreterContext(String input, String firstRule) {
            this(input, 4 * input.length());
            this.pendingRules.addFirst(firstRule);
        }

        @SuppressWarnings("CopyConstructorMissesField")
        InterpreterContext(InterpreterContext other) {
            this(other.input, other.maxBranches - 1);
            this.position = other.position;
            this.pendingRules.addAll(other.pendingRules);
            this.appliedRules.addAll(other.appliedRules);
        }
    }

    static class MessageInterpreter {
        final Map<Integer, String> rules;

        MessageInterpreter(Map<Integer, String> rules) {
            this.rules = rules;
        }

        List<InterpreterContext> process(InterpreterContext context) {
            if (context.pendingRules.isEmpty()) {
                return Collections.emptyList();
            }

            String rule = context.pendingRules.removeFirst();
            context.appliedRules.add(rule);

            final Matcher constant = constantPattern.matcher(rule);
            if (constant.matches()) {
                return parseConstant(context, constant.group("value"));
            }

            final Matcher union = unionPattern.matcher(rule);
            if (union.matches()) {
                return parseUnion(context, union.group("left"), union.group("right"));
            }

            final Matcher concatenation = concatenationPattern.matcher(rule);
            if (concatenation.matches()) {
                return parseConcatenation(context, rule);
            }

            throw new IllegalArgumentException("Invalid rule: " + rule);
        }

        private List<InterpreterContext> parseConcatenation(InterpreterContext context, String rule) {
            final List<String> rules = Arrays.stream(rule.split(" "))
                    .map(Integer::valueOf)
                    .map(this.rules::get)
                    .collect(Collectors.toList());
            reverseList(rules).forEach(context.pendingRules::addFirst);
            return Collections.singletonList(context);
        }

        private List<InterpreterContext> parseUnion(InterpreterContext context, String left, String right) {
            if (context.maxBranches < 0) {
                return Collections.emptyList();
            }

            InterpreterContext leftContext = new InterpreterContext(context);
            leftContext.pendingRules.addFirst(left);
            InterpreterContext rightContext = new InterpreterContext(context);
            rightContext.pendingRules.addFirst(right);
            return List.of(leftContext, rightContext);
        }

        private List<InterpreterContext> parseConstant(InterpreterContext context, String value) {
            int endPosition = context.position + value.length();
            if (endPosition > context.input.length()) {
                return Collections.emptyList();
            }

            boolean result = value.equals(context.input.substring(
                    context.position,
                    endPosition
            ));

            if (result) {
                context.position = endPosition;
                return Collections.singletonList(context);
            }

            return Collections.emptyList();
        }

        boolean isValid(String input) {
            final Stack<InterpreterContext> stack = new Stack<>();
            stack.push(new InterpreterContext(input, rules.get(0)));
            while (!stack.empty()) {
                InterpreterContext context = stack.pop();
                List<InterpreterContext> result = process(context);
                if (result.stream()
                        .anyMatch(c -> c.position == input.length() && c.pendingRules.isEmpty())
                ) {
                    return true;
                }
                stack.addAll(result);
            }
            return false;
        }
    }

    public Day19(List<String> input) {
        final List<List<String>> blocks = splitByBlankLine(input);

        rules = blocks.get(0).stream()
                .map(this::parseRule)
                .collect(Collectors.toMap(Pair::getKey, Pair::getValue));

        messages = blocks.get(1);
    }

    private Pair<Integer, String> parseRule(String input) {
        final Matcher matcher = rulePattern.matcher(input);
        if (!matcher.matches()) {
            throw new IllegalArgumentException("Invalid rule: " + input);
        }

        final Integer index = Integer.valueOf(matcher.group("index"));
        final String conditions = matcher.group("conditions");
        return Pair.of(index, conditions);
    }

    public List<String> getValidMessages() {
        MessageInterpreter interpreter = new MessageInterpreter(rules);
        return messages.stream()
                .filter(interpreter::isValid)
                .collect(Collectors.toList());
    }
}
