package adventofcode2020;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Day17 {

    enum CellState {
        ACTIVE,
        INACTIVE;

        public static final EnumMapping<CellState> mapping = new EnumMapping<>(Map.of(
                ACTIVE, '#',
                INACTIVE, '.'
        ));
    }

    static class Coordinate implements Cloneable {
        final int[] values;

        public Coordinate(int dimensions) {
            this.values = new int[dimensions];
        }

        public Coordinate(int... args) {
            this(args.length);
            System.arraycopy(args, 0, values, 0, args.length);
        }

        @Override
        @SuppressWarnings("MethodDoesntCallSuperMethod")
        public Coordinate clone() {
            Coordinate clone = new Coordinate(values.length);
            System.arraycopy(values, 0, clone.values, 0, values.length);
            return clone;
        }

        private Coordinate dec(int index) {
            values[index] -= 1;
            return this;
        }

        private Coordinate inc(int index) {
            values[index] += 1;
            return this;
        }

        private Stream<Coordinate> getNeighbors() {
            return getNeighborsForDimensions(values.length).filter(coordinate -> !coordinate.equals(this));
        }

        private Stream<Coordinate> getNeighborsForDimensions(int dimensions) {
            if (dimensions < 1) {
                throw new IllegalArgumentException("you should be more positive: " + dimensions);
            }

            if (dimensions == 1) {
                return Stream.of(clone().dec(0), clone(), clone().inc(0));
            }

            return Stream.of(
                    getNeighborsForDimensions(dimensions - 1).map(coordinate -> coordinate.dec(dimensions - 1)),
                    getNeighborsForDimensions(dimensions - 1),
                    getNeighborsForDimensions(dimensions - 1).map(coordinate -> coordinate.inc(dimensions - 1))
            ).reduce(Stream::concat).orElseThrow();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (o == null || getClass() != o.getClass()) return false;

            Coordinate that = (Coordinate) o;

            return Arrays.equals(values, that.values);
        }

        @Override
        public int hashCode() {
            return Arrays.hashCode(values);
        }

        @Override
        public String toString() {
            return Arrays.stream(values)
                    .mapToObj(String::valueOf)
                    .collect(Collectors.joining(", "));
        }
    }

    static class Grid {
        final Set<Coordinate> activeCells = new HashSet<>();
        private final int dimensions;

        public Grid(int dimensions) {
            this.dimensions = dimensions;
        }

        public void step() {
            final var neighborCountMap = new HashMap<Coordinate, Integer>();
            activeCells.forEach(cell -> cell.getNeighbors().
                    forEach(neighborCell -> neighborCountMap.put(
                            neighborCell, neighborCountMap.getOrDefault(neighborCell, 0) + 1
                    ))
            );

            activeCells.removeAll(activeCells.stream()
                    .filter(cell -> {
                        final int numNeighbors = neighborCountMap.getOrDefault(cell, 0);
                        return numNeighbors < 2 || numNeighbors > 3;
                    })
                    .collect(Collectors.toList())
            );
            activeCells.addAll(
                    neighborCountMap.entrySet().stream()
                            .filter(entry -> entry.getValue() == 3)
                            .map(Map.Entry::getKey)
                            .collect(Collectors.toList())
            );
        }

        public static Grid parse(int dimensions, List<String> input) {
            final Grid grid = new Grid(dimensions);
            grid.activeCells.addAll(
                    IntStream.range(0, input.size())
                            .mapToObj(y -> parseLine(dimensions, y, input.get(y)))
                            .reduce(Stream::concat)
                            .orElseThrow()
                            .collect(Collectors.toList())
            );
            return grid;
        }

        public static Grid parse(List<String> input) {
            return parse(3, input);
        }

        private static Stream<Coordinate> parseLine(int dimensions, int y, String line) {
            return IntStream.range(0, line.length())
                    .filter(x -> CellState.mapping.parse(line.charAt(x)) == CellState.ACTIVE)
                    .mapToObj(x -> {
                        final int[] values = new int[dimensions];
                        values[0] = x;
                        values[1] = y;
                        return new Coordinate(values);
                    });
        }

        @Override
        public String toString() {
            if (dimensions != 3) {
                throw new UnsupportedOperationException("Developer was too tired to implement this...");
            }

            final Coordinate min = new Coordinate(dimensions);
            final Coordinate max = new Coordinate(dimensions);

            IntStream.range(0, dimensions).forEach(i -> {
                min.values[i] = activeCells.stream().mapToInt(cell -> cell.values[i]).min().orElseThrow();
                max.values[i] = activeCells.stream().mapToInt(cell -> cell.values[i]).max().orElseThrow();
            });

            final StringBuilder builder = new StringBuilder();
            for (int z = min.values[2]; z <= max.values[2]; z++) {
                builder.append("z=").append(z).append("\n");
                for (int y = min.values[1]; y <= max.values[1]; y++) {
                    for (int x = min.values[0]; x <= max.values[0]; x++) {
                        if (activeCells.contains(new Coordinate(x, y, z))) {
                            builder.append(CellState.mapping.encode(CellState.ACTIVE));
                        } else {
                            builder.append(CellState.mapping.encode(CellState.INACTIVE));
                        }
                    }

                    if (z < max.values[2] || y < max.values[1]) {
                        builder.append("\n");
                    }
                }

                if (z < max.values[2]) {
                    builder.append("\n");
                }
            }
            return builder.toString();
        }

        public int countActiveCells() {
            return activeCells.size();
        }
    }
}
