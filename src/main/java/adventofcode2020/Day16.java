package adventofcode2020;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.*;
import java.util.function.Supplier;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static adventofcode2020.Utils.splitByBlankLine;

public class Day16 {

    private static final Pattern attributePattern = Pattern.compile("^(?<name>[a-z ]+): (?<range1>\\d+-\\d+) or (?<range2>\\d+-\\d+)$");
    private static final Pattern rangePattern = Pattern.compile("^(?<min>\\d+)-(?<max>\\d+)$");

    final Map<String, Supplier<IntStream>> attributeRanges = new HashMap<>();
    final List<Integer> yourTicket;
    List<List<Integer>> nearbyTickets;

    public Day16(List<String> input) {
        final List<List<String>> blocks = splitByBlankLine(input);
        if (blocks.size() != 3) {
            throw new IllegalArgumentException("Unknown blocks!");
        }

        final List<String> attributeList = blocks.get(0);
        attributeList.stream()
                .map(Day16::parseAttribute)
                .forEach(pair -> attributeRanges.put(pair.getKey(), pair.getValue()));

        yourTicket = parseTicket(blocks.get(1).get(1));

        final List<String> nearbyTicketLines = blocks.get(2);
        nearbyTickets = nearbyTicketLines.stream().skip(1).map(Day16::parseTicket).collect(Collectors.toList());
    }

    public IntStream findInvalidNearbyTicketValues() {
        return nearbyTickets.stream()
                .map(this::findInvalidValues)
                .reduce(IntStream::concat)
                .orElseThrow();
    }

    IntStream findInvalidValues(List<Integer> ticket) {
        return ticket.stream()
                .filter(value -> attributeRanges.values().stream().noneMatch(
                        createRange -> createRange.get().anyMatch(i -> i == value)
                ))
                .mapToInt(Integer::intValue);
    }

    private static List<Integer> parseTicket(String input) {
        return Arrays.stream(input.split(","))
                .map(Integer::valueOf)
                .collect(Collectors.toList());
    }

    public static Pair<String, Supplier<IntStream>> parseAttribute(String input) {
        final Matcher matcher = attributePattern.matcher(input);
        if (!matcher.find()) {
            throw new IllegalArgumentException("Invalid attribute line: " + input);
        }
        return Pair.of(
                matcher.group("name"),
                () -> IntStream.concat(
                        parseRange(matcher.group("range1")),
                        parseRange(matcher.group("range2"))
                )
        );
    }

    private static IntStream parseRange(String input) {
        final Matcher matcher = rangePattern.matcher(input);
        if (!matcher.find()) {
            throw new IllegalArgumentException("Invalid range: " + input);
        }
        return IntStream.rangeClosed(Integer.parseInt(matcher.group("min")), Integer.parseInt(matcher.group("max")));
    }

    public void discardInvalidTickets() {
        nearbyTickets = nearbyTickets.stream()
                .filter(ticket -> findInvalidValues(ticket).count() == 0)
                .collect(Collectors.toList());
    }

    public List<String> findAttributeOrder() {
        final List<Set<String>> attributeCandidates = IntStream.range(0, attributeRanges.size())
                .mapToObj(this::findAttributeCandidates)
                .collect(Collectors.toCollection(ArrayList::new));

        for (int i = 0; i < attributeRanges.size(); i++) {
            final List<String> fixedNames = attributeCandidates.stream()
                    .filter(set -> set.size() == 1)
                    .map(set -> set.iterator().next())
                    .collect(Collectors.toList());
            if (fixedNames.size() == attributeRanges.size()) {
                return fixedNames;
            }

            attributeCandidates.stream()
                    .filter(set -> set.size() > 1)
                    .forEach(set -> set.removeAll(fixedNames));
        }

        throw new RuntimeException("Did not find a unique solution!");
    }

    private Set<String> findAttributeCandidates(int attributeIndex) {
        return attributeRanges.entrySet().stream()
                .filter(entry -> nearbyTickets.stream().allMatch(ticket -> {
                    final IntStream validRange = entry.getValue().get();
                    return validRange.anyMatch(i -> i == ticket.get(attributeIndex));
                }))
                .map(Map.Entry::getKey)
                .collect(Collectors.toCollection(HashSet::new));
    }
}
