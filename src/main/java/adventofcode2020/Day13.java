package adventofcode2020;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day13 {
    public static class Departures {

        public final int desiredDepartureTime;
        public final Map<Integer, Integer> busLines = new HashMap<>();

        public Departures(List<String> input) {

            desiredDepartureTime = Integer.parseInt(input.get(0));
            String[] busLineValues = input.get(1).split(",");
            for (int i = 0; i < busLineValues.length; i++) {
                String value = busLineValues[i];
                if (!value.equals("x")) {
                    busLines.put(Integer.valueOf(value), i);
                }
            }
        }

        public int findBestBus() {
            int minimumBusLine = Collections.min(busLines.keySet());
            return IntStream.range(desiredDepartureTime, desiredDepartureTime + minimumBusLine)
                    .mapToObj(departure ->
                            busLines.keySet().stream()
                                    .filter(line -> departure % line == 0)
                                    .findFirst()
                                    .orElse(null)
                    )
                    .filter(Objects::nonNull)
                    .findFirst()
                    .orElseThrow();
        }

        public int getWaitingTime() {
            int bestBus = findBestBus();
            return bestBus - (desiredDepartureTime % bestBus);
        }

        // Chinese remainder theorem... or at least something similar
        public long findBusOffsetDeparture() {
            List<Map.Entry<Integer, Integer>> entryList = new ArrayList<>(busLines.entrySet());
            List<Integer> remainders = entryList.stream().map(Map.Entry::getValue).collect(Collectors.toList());
            List<Integer> modules = entryList.stream().map(Map.Entry::getKey).collect(Collectors.toList());
            long moduleProduct = modules.stream().mapToLong(Integer::longValue).reduce((a, b) -> a * b).orElseThrow();
            List<Long> factors = modules.stream()
                    .mapToLong(module ->
                            IntStream.range(1, Integer.MAX_VALUE)
                                    .filter(i -> (i * moduleProduct / module) % module == 1)
                                    .findFirst()
                                    .orElseThrow()
                    )
                    .boxed()
                    .collect(Collectors.toList());

            long solution = IntStream.range(0, entryList.size())
                    .mapToLong(i -> moduleProduct * remainders.get(i) * factors.get(i) / modules.get(i))
                    .sum() % moduleProduct;
            return moduleProduct - solution;
        }
    }
}
