package adventofcode2020;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Day02 {

    private static final Pattern passwordPattern = Pattern.compile("^(\\d+)-(\\d+) (\\w): (.+)$");

    public static boolean checkPasswordCount(String passwordLine) {
        Matcher matcher = parsePassword(passwordLine);

        int min = Integer.parseInt(matcher.group(1));
        int max = Integer.parseInt(matcher.group(2));
        
        char character = matcher.group(3).charAt(0);
        String password = matcher.group(4);

        long count = password.chars().filter(c -> c == character).count();

        if(count>=min && count <= max){
            return true;
        }
        return false;
    }

    private static Matcher parsePassword(String passwordLine) {
        Matcher matcher = passwordPattern.matcher(passwordLine);
        if(!matcher.find()) {
            throw new IllegalArgumentException(passwordLine + " does not match " + passwordPattern.pattern());
        }
        return matcher;
    }

    public static long checkCharacterCountList(List<String> passwordList) {
        return passwordList.stream()
                .filter(Day02::checkPasswordCount)
                .count();
    }


    public static boolean checkPasswordPosition(String stringLine){

        Matcher matcher = parsePassword(stringLine);

        int positionFirst = Integer.parseInt(matcher.group(1));
        int positionSecond = Integer.parseInt(matcher.group(2));

        char character = matcher.group(3).charAt(0);
        String password = matcher.group(4);

        if (password.charAt(positionFirst-1) == character) {
            return password.charAt(positionSecond - 1) != character;
        }
        return password.charAt(positionSecond - 1) == character;
    }


    public static long checkCharacterPositionList(List<String> passwordList) {
        return passwordList.stream()
                .filter(Day02::checkPasswordPosition)
                .count();
    }
}
