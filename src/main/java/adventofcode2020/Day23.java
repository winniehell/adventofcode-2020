package adventofcode2020;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day23 {
    static class CrabCups {

        final LinkedList<Integer> cups;
        Integer currentCupValue;

        CrabCups(String input) {
            this.cups = Arrays.stream(input.split(""))
                    .map(Integer::valueOf)
                    .collect(Collectors.toCollection(LinkedList::new));
            this.currentCupValue = cups.peek();
        }

        void nextMove() {
            final List<Integer> pickedUpCups = IntStream.range(0, 3)
                    .mapToObj(i -> {
                        final int pickUpIndex = wrapIndex(cups.indexOf(currentCupValue) + 1);
                        return cups.remove(pickUpIndex);
                    })
                    .collect(Collectors.toList());

            int destinationCupValue = currentCupValue - 1;
            while (!cups.contains(destinationCupValue)) {
                destinationCupValue -= 1;
                if (destinationCupValue < 1) {
                    destinationCupValue = Collections.max(cups);
                    break;
                }
            }

            int destinationCupIndex = cups.indexOf(destinationCupValue);
            for (int i = 0; i < pickedUpCups.size(); i++) {
                cups.add(destinationCupIndex + i + 1, pickedUpCups.get(i));
            }

            final int currentCupIndex = cups.indexOf(currentCupValue);
            currentCupValue = cups.get(wrapIndex(currentCupIndex + 1));
        }

        int wrapIndex(int index) {
            if (index < cups.size()) {
                return index;
            }
            return index % cups.size();
        }

        void run(int numMoves) {
            for (int i = 0; i < numMoves; i++) {
                nextMove();
            }
        }
    }
}
