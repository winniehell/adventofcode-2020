package adventofcode2020;

import java.util.*;
import java.util.stream.Collectors;

public class Day04 {

    private final List<Map<String, String>> passportList;
    private final Set<String> requiredFields = Set.of(
            "byr",
            "iyr",
            "eyr",
            "hgt",
            "hcl",
            "ecl",
            "pid"
    );

    private Map<String, String> parseAttributes(String line) {
        Map<String, String> attributes = new HashMap<>();
        String[] entries = line.split(" ");
        Arrays.stream(entries).forEach(entry -> {
            String[] keyValuePair = entry.split(":");
            attributes.put(keyValuePair[0], keyValuePair[1]);
        });

        return attributes;
    }

    public Day04(List<String> input) {
        this.passportList = Utils.splitByBlankLine(input).stream()
                .map(lines -> String.join(" ", lines))
                .map(this::parseAttributes)
                .collect(Collectors.toList());
    }

    public boolean hasRequiredFields(Map<String, String> passport) {
        return passport.keySet().containsAll(requiredFields);
    }

    public long countPassportsWithRequiredFields() {
        return this.passportList.stream().filter(this::hasRequiredFields).count();
    }

    public boolean isValidPassport(Map<String, String> passport) {

        try {


            int birthYear = Integer.parseInt(passport.get("byr"));
            if (1920 > birthYear || birthYear > 2002) {
                return false;
            }

            int issueYear = Integer.parseInt(passport.get("iyr"));
            if (2010 > issueYear || issueYear > 2020) {
                return false;
            }

            int expirationYear = Integer.parseInt(passport.get("eyr"));
            if (2020 > expirationYear || expirationYear > 2030) {
                return false;
            }

            String height = passport.get("hgt");
            String heightUnit = height.substring(height.length() - 2);
            int heightValue = Integer.parseInt(height.substring(0, height.length() - 2));

            if (heightUnit.equals("in")) {
                if (59 > heightValue || heightValue > 76) {
                    return false;
                }
            } else if (heightUnit.equals("cm")) {
                if (150 > heightValue || heightValue > 193) {
                    return false;
                }
            } else {
                return false;
            }

            String hairColor = passport.get("hcl");
            if (!hairColor.matches("^#[0-9a-fA-F]{6}$")) {
                return false;
            }

            String eyeColor = passport.get("ecl");
            if (!Set.of("amb", "blu", "brn", "gry", "grn", "hzl", "oth").contains(eyeColor)) {
                return false;
            }

            String passportId = passport.get("pid");
            if (!passportId.matches("^[0-9]{9}$")) {
                return false;
            }
        } catch (Exception e) {
            System.out.println(e);
            return false;
        }

        return true;
    }

    public long countValidPassports() {
        return this.passportList.stream().filter(this::isValidPassport).count();
    }
}
