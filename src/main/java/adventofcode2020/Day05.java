package adventofcode2020;

import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Day05 {

    private static int center(Range<Integer> range) {
        return (range.getMinimum() + range.getMaximum()) / 2;
    }

    static class BoardingPass {
        int row;
        int column;

        public BoardingPass(int row, int column) {
            this.row = row;
            this.column = column;
        }

        public static BoardingPass parse(String input) {
            Range<Integer> rowRange = Range.between(0, 127);
            Range<Integer> columnRange = Range.between(0, 7);
            for (char c : input.toCharArray()) {
                switch (c) {
                    case 'F' -> rowRange = lowerHalf(rowRange);
                    case 'B' -> rowRange = uppperHalf(rowRange);
                    case 'L' -> columnRange = lowerHalf(columnRange);
                    case 'R' -> columnRange = uppperHalf(columnRange);
                    default -> throw new IllegalArgumentException("Unexpected character: " + c);
                }
            }
            return new BoardingPass(rowRange.getMinimum(), columnRange.getMinimum());
        }

        private static Range<Integer> uppperHalf(Range<Integer> range) {
            return Range.between(center(range) + 1, range.getMaximum());
        }

        private static Range<Integer> lowerHalf(Range<Integer> range) {
            return Range.between(range.getMinimum(), center(range));
        }

        public int getSeatId() {
            return 8 * row + column;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (o == null || getClass() != o.getClass()) return false;

            BoardingPass that = (BoardingPass) o;

            return new EqualsBuilder().append(row, that.row).append(column, that.column).isEquals();
        }

        @Override
        public int hashCode() {
            return Objects.hash(row, column);
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("row", row)
                    .append("column", column)
                    .toString();
        }
    }

    private final List<BoardingPass> boardingPasses;

    public Day05(List<String> encodedBoardingPasses) {
        this.boardingPasses = encodedBoardingPasses.stream()
                .map(BoardingPass::parse)
                .collect(Collectors.toList());
    }

    public int highestSeatID() {
        return this.boardingPasses.stream()
                .mapToInt(BoardingPass::getSeatId)
                .max()
                .orElse(Integer.MIN_VALUE);
    }

    public List<Integer> getEmptySeats() {
        final List<Integer> occupiedSeats = getOccupiedSeats();

        var maximumSeatId = 8 * 127 + 7;
        return IntStream.range(0, maximumSeatId)
                .filter(i -> !occupiedSeats.contains(i))
                .boxed()
                .collect(Collectors.toList());
    }

    public List<Integer> getOccupiedSeats() {
        return this.boardingPasses.stream()
                .map(BoardingPass::getSeatId)
                .collect(Collectors.toList());
    }
}
