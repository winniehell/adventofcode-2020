package adventofcode2020;

import org.apache.commons.lang3.builder.EqualsBuilder;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static adventofcode2020.Utils.splitByBlankLine;

public class Day22 {
    static class Player {
        final int id;
        final Deque<Integer> cardStack;

        Player(int id, List<Integer> cards) {
            this.id = id;
            this.cardStack = new ArrayDeque<>();
            cardStack.addAll(cards);
        }

        Player(List<String> input) {
            this(
                    Integer.parseInt(
                            input.get(0)
                                    .replace("Player ", "")
                                    .replace(":", "")
                    ),
                    input.subList(1, input.size()).stream()
                            .map(Integer::valueOf)
                            .collect(Collectors.toList())
            );
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;

            if (o == null || getClass() != o.getClass()) return false;

            Player player = (Player) o;

            return new EqualsBuilder().append(id, player.id).append(cardStack, player.cardStack).isEquals();
        }

        @Override
        public int hashCode() {
            return Objects.hash(id, cardStack);
        }

        @Override
        public String toString() {
            final StringBuilder builder = new StringBuilder();
            builder.append("Player ");
            builder.append(id);
            builder.append(":");
            builder.append("\n");
            cardStack.forEach(card -> {
                builder.append(card);
                builder.append("\n");
            });
            builder.replace(
                    builder.length() - 1,
                    builder.length(),
                    ""
            );
            return builder.toString();
        }

        int getScore() {
            final List<Integer> multipliers = IntStream.range(0, cardStack.size())
                    .mapToObj(i -> (cardStack.size() - i))
                    .collect(Collectors.toList());
            return cardStack.stream()
                    .mapToInt(card -> card * multipliers.remove(0))
                    .sum();
        }

        public String getHistoryEntry() {
            return id + ":" + cardStack.stream()
                    .map(String::valueOf)
                    .collect(Collectors.joining(","));
        }
    }

    static class Combat {
        final List<Player> players;
        final Set<String> history;

        @SuppressWarnings("unused")
        Combat(List<Player> players, boolean javaDoesNotLikeOverloadedConstructors) {
            this.players = players;
            history = new HashSet<>();
        }

        Combat(List<String> input) {
            this.players = splitByBlankLine(input).stream()
                    .map(Player::new)
                    .collect(Collectors.toList());
            history = new HashSet<>();
        }

        void nextRound() {
            final List<Integer> cards = drawCards();
            final int winningCard = Collections.max(cards);
            winRound(cards, winningCard);
        }

        void winRound(List<Integer> cards, int winningCard) {
            final Player winningPlayer = players.get(cards.indexOf(winningCard));
            winningPlayer.cardStack.addLast(winningCard);
            cards.stream()
                    .filter(c -> c != winningCard)
                    .forEach(winningPlayer.cardStack::addLast);
        }

        List<Integer> drawCards() {
            return players.stream()
                    .map(p -> p.cardStack.removeFirst())
                    .collect(Collectors.toList());
        }

        void run() {
            while (!isFinished()) {
                nextRound();
            }
        }

        boolean isFinished() {
            return players.stream().anyMatch(p -> p.cardStack.isEmpty());
        }

        public int getWinningScore() {
            return players.stream()
                    .mapToInt(Player::getScore)
                    .max()
                    .orElseThrow();
        }
    }

    static class RecursiveCombat extends Combat {
        RecursiveCombat(List<Player> players, boolean javaDoesNotLikeOverloadedConstructors) {
            super(players, javaDoesNotLikeOverloadedConstructors);
        }

        RecursiveCombat(List<String> input) {
            super(input);
        }

        @Override
        void nextRound() {
            final String historyEntry = players.stream()
                    .map(Player::getHistoryEntry)
                    .collect(Collectors.joining("|"));

            if (history.contains(historyEntry)) {
                preventInfiniteLoop();
                return;
            }

            history.add(historyEntry);

            final List<Integer> cards = drawCards();
            final List<Integer> stackSizes = players.stream()
                    .map(p -> p.cardStack.size())
                    .collect(Collectors.toList());
            final List<Integer> winningCards = IntStream.range(0, cards.size())
                    .filter(i -> stackSizes.get(i) >= cards.get(i))
                    .mapToObj(cards::get)
                    .collect(Collectors.toList());

            if (winningCards.size() < cards.size()) {
                final int winningCard = Collections.max(cards);
                winRound(cards, winningCard);
                return;
            }

            final List<Player> subgamePlayers = IntStream.range(0, players.size())
                    .mapToObj(i -> {
                        final Player player = players.get(i);
                        final List<Integer> subgameCards = List.copyOf(player.cardStack).subList(0, cards.get(i));
                        return new Player(player.id, subgameCards);
                    })
                    .collect(Collectors.toList());

            final RecursiveCombat subgame = new RecursiveCombat(subgamePlayers, true);
            subgame.run();
            final int subgameWinningScore = subgame.getWinningScore();
            final List<Player> subgameWinners = subgamePlayers.stream()
                    .filter(p -> p.getScore() == subgameWinningScore)
                    .collect(Collectors.toList());
            if (subgameWinners.size() != 1) {
                throw new RuntimeException("we have a problem here...");
            }
            final int winningCard = cards.get(
                    subgamePlayers.indexOf(subgameWinners.get(0))
            );

            winRound(cards, winningCard);
        }

        void preventInfiniteLoop() {
            final Player firstPlayer = players.get(0);
            players.subList(1, players.size()).forEach(player -> {
                firstPlayer.cardStack.addAll(player.cardStack);
                player.cardStack.clear();
            });
        }
    }
}
