package adventofcode2020;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Utils {
    public static <T> Set<T> intersection(List<Set<T>> input) {
        return input.stream().reduce(union(input), (a, b) -> {
            a.retainAll(b);
            return a;
        });
    }

    public static List<List<String>> splitByBlankLine(List<String> input) {
        final ArrayList<List<String>> result = new ArrayList<>();

        final Stack<String> stack = new Stack<>();
        input.forEach(line -> {
            if (line.isEmpty()) {
                result.add(List.copyOf(stack));
                stack.clear();
            } else {
                stack.push(line);
            }
        });

        if (!stack.empty()) {
            result.add(List.copyOf(stack));
        }

        return result;
    }

    public static <T> Set<T> union(List<Set<T>> input) {
        return input.stream().reduce(new HashSet<>(), (a, b) -> {
            a.addAll(b);
            return a;
        });
    }

    public static String getColumn(List<String> rows, int columnIndex) {
        return rows.stream()
                .map(line -> line.substring(columnIndex, columnIndex + 1))
                .collect(Collectors.joining());
    }

    public static String reverse(String input) {
        return IntStream.range(0, input.length())
                .mapToObj(i -> String.valueOf(input.charAt(input.length() - i - 1)))
                .collect(Collectors.joining());
    }

    public static List<String> rotateGrid(List<String> grid) {
        int numColumns = grid.stream().mapToInt(String::length).max().orElseThrow();
        List<String> columns = IntStream.range(0, numColumns)
                .mapToObj(index -> getColumn(grid, index))
                .collect(Collectors.toList());
        return columns.stream().map(Utils::reverse).collect(Collectors.toList());
    }

    public static <T> List<T> reverseList(List<T> list) {
        return IntStream.range(0, list.size())
                .mapToObj(i -> list.get(list.size() - 1 - i))
                .collect(Collectors.toList());
    }

    public static List<String> concatenateRows(List<List<String>> grid) {
        int numRows = grid.stream().mapToInt(List::size).max().orElseThrow();
        return IntStream.range(0, numRows).mapToObj(rowIndex ->
                grid.stream().map(column -> {
                    if (rowIndex > column.size() - 1) {
                        return "";
                    }
                    return column.get(rowIndex);
                })
                        .collect(Collectors.joining())
        )
                .collect(Collectors.toList());
    }
}
