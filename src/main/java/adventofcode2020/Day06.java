package adventofcode2020;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Day06 {

    private final List<List<Set<Integer>>> groups;

    public Day06(List<String> input) {
        this.groups = Utils.splitByBlankLine(input).stream()
                .map(lines -> lines.stream()
                        .map(String::chars)
                        .map(IntStream::boxed)
                        .map(s -> s.collect(Collectors.toUnmodifiableSet()))
                        .collect(Collectors.toList())
                )
                .collect(Collectors.toList());
    }

    public long countUnionAnswers() {
        return this.groups.stream()
                .map(Utils::union)
                .mapToLong(Set::size)
                .sum();
    }

    public long countIntersectionAnswers() {
        return this.groups.stream()
                .map(Utils::intersection)
                .mapToLong(Set::size)
                .sum();
    }
}
