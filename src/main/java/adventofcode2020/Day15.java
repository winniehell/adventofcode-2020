package adventofcode2020;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Day15 {

    private int index;
    private int lastNumber;
    private final Map<Integer, Integer> indexMap = new HashMap<>();

    public Day15(List<Integer> startSequence) {
        index = 0;
        lastNumber = startSequence.get(0);
        startSequence.subList(1, startSequence.size()).forEach(this::addNumber);
    }

    public void addNumber(int newNumber) {
        indexMap.put(lastNumber, index);
        index++;
        lastNumber = newNumber;
    }

    public void step() {
        final int nextNumber = index - indexMap.getOrDefault(lastNumber, index);
        addNumber(nextNumber);
    }

    public int runUntil(int length) {
        while (index < length - 1) {
            step();
        }
        return lastNumber;
    }
}
