import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Day21 {

    static class Food {
        static Pattern foodPattern = Pattern.compile(
                "(?<ingredients>[a-z ]+) \\(contains (?<allergens>[a-z, ]+)\\)"
        );

        final List<String> ingredients;
        final List<String> allergens;

        Food(List<String> ingredients, List<String> allergens) {
            this.ingredients = ingredients;
            this.allergens = allergens;
        }

        static Food parse(String input) {
            Matcher matcher = foodPattern.matcher(input);
            if (!matcher.matches()) {
                throw new IllegalArgumentException("Illegal food: " + input);
            }
            List<String> ingredients = Arrays.asList(matcher.group("ingredients").split(" "));
            List<String> allergens = Arrays.asList(matcher.group("allergens").split(", "));
            return new Food(ingredients, allergens);
        }
    }

    static class FoodList {
        final List<Food> foods;
        final SortedMap<String, Set<String>> allergicCandidates;

        FoodList(List<String> input) {
            foods = input.stream()
                    .map(Food::parse)
                    .collect(Collectors.toList());

            allergicCandidates = new TreeMap<>();
            foods.forEach(food -> food.allergens.forEach(allergen -> {
                Set<String> previousCandidates = allergicCandidates.get(allergen);
                if (previousCandidates == null) {
                    allergicCandidates.put(allergen, new HashSet<>(food.ingredients));
                } else {
                    previousCandidates.retainAll(food.ingredients);
                }
            }));
        }

        Set<String> findNonAllergicIngredients() {
            List<String> allIngredients = getAllIngredients();
            return allIngredients.stream()
                    .filter(ingredient ->
                            allergicCandidates.values().stream()
                                    .noneMatch(candidates -> candidates.contains(ingredient))
                    )
                    .collect(Collectors.toSet());
        }

        List<String> getAllIngredients() {
            return foods.stream()
                    .map(food -> food.ingredients.stream())
                    .reduce(Stream::concat)
                    .orElseThrow()
                    .collect(Collectors.toList());
        }

        long countFoods(String ingredient) {
            return foods.stream()
                    .filter(food -> food.ingredients.contains(ingredient))
                    .count();
        }

        List<String> findAllergicIngredients() {
            for (int i = 0; i < allergicCandidates.size(); i++) {
                Map<String, String> resolvedCandidates = getResolvedCandidates();
                allergicCandidates.forEach((allergen, ingredients) -> {
                    if (resolvedCandidates.containsKey(allergen)) {
                        return;
                    }
                    ingredients.removeAll(resolvedCandidates.values());
                });

                if (resolvedCandidates.size() == allergicCandidates.size()) {
                    break;
                }
            }

            return allergicCandidates.values().stream()
                    .map(strings -> strings.iterator().next())
                    .collect(Collectors.toList());
        }

        Map<String, String> getResolvedCandidates() {
            return allergicCandidates.entrySet().stream()
                    .filter(entry -> entry.getValue().size() == 1)
                    .collect(Collectors.toMap(
                            Map.Entry::getKey,
                            entry -> entry.getValue().iterator().next()
                    ));
        }
    }
}
